#!/usr/bin/env bash
#SBATCH -a 0-499
#SBATCH -t 1:00:00
#SBATCH -p gpu
#SBATCH -c 1
#SBATCH -G 4


source ~/.bashrc
module load cuda
nvidia-smi
conda activate torch37
python CreateRecsRectBash.py $SLURM_ARRAY_TASK_ID