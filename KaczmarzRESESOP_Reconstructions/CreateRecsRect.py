import Resesop
import numpy as np
import matplotlib.pyplot as plt
import torch


NumOfPhantoms = 1
# NumOfAngles = 450
NumOfOffsetvalues = 300

for i in range(NumOfPhantoms):
    print('Phantom: ' + str(i+1))

    phantom = np.load('../Phantoms/Images/DataRect_npy/phantom_' + str(i + 1) + '.npy')
    sinogram_p = np.load('../Sinograms/DataRect_p_npy/0testsino_p_' + str(i + 1) + '.npy')
    sinogram = np.load('../Sinograms/DataRect_npy/0testsino_' + str(i + 1) + '.npy')
    sinogram_end = np.load('../Sinograms/DataRect_end_npy/0testsino_' + str(i + 1) + '.npy')

    delta = np.zeros_like(sinogram)
    for t in [0, np.shape(sinogram)[0]-1]:
        print('time:' + str(int(t)))
        if t == 0:
            eta = np.absolute(sinogram-sinogram_p)
        else:
            assert t == np.shape(sinogram)[0]-1
            eta = np.absolute(sinogram_end-sinogram_p)

        rec, stopped = Resesop.run_resesop(sinogram_p, eta, delta, reconstruction_size=128, max_iterations=3)
        np.save('DataRect_npy/0testrec_' + str(i + 1) + 't=' + str(int(t)), rec)
        plt.imshow(rec, cmap='gray', vmin=0, vmax=1)
        plt.savefig('DataRect_png/0testrec_' + str(i + 1) + 't=' + str(int(t)))
        plt.close()
        if t == 0:
            rec_pt = torch.from_numpy(rec)
            rec_pt = torch.unsqueeze(rec_pt, 0)
            torch.save(rec_pt, '../LandmarkDetectionSmallerImages/TrainingImages2'
                              '/0testimage_' + str(i + 1) + '.pt')
        print('done')