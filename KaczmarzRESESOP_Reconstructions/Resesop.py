import numpy as np
import sys

sys.path.insert(0, '/home/uni08/gsarnig/masterarbeit/Sinograms')
sys.path.insert(0, 'C:\\Users\\gesas\\PycharmProjects\\masterarbeit\\Sinograms')
import sino




# discrete L2 scalar product for matrices x,y with NxN pixels
def discreteL2product(x, y):
    return 4 / np.shape(x)[0] ** 2 * np.sum(x * y)


# run_resesop implements the fully discretized RESESOP-Kaczmarz method with KxL Kaczmarz loops and no averaging

# sinogram_p: perturbed sinogram of size KxL
# eta: model error for each source position and offsetvalue of size KxL
# delta: data error for each source position and offsetvalue of size KxL
# tau: constant for discrepancy principle
def run_resesop(sinogram_p, eta, delta, tau=1.00001, reconstruction_size=512, max_iterations=10, two_search_directions=False, not_bigger_1=True):
    stopped = np.ones_like(sinogram_p)
    N = reconstruction_size
    f = np.zeros((N, N))  # iterates of reconstructed image
    K = sinogram_p.shape[0]  # number of source positions (phi)
    phi_List = np.linspace(0, np.pi, num=K, endpoint=False)
    L = sinogram_p.shape[1]  # number of detector points (s)
    s_List = np.linspace(-1, 1, num=L, endpoint=True)
    n = 0  # iteration index

    # initialize alphas, xis and us
    u_old = np.zeros_like(f)
    norm2u_old = 0  # ||u_old||^2
    u_new = np.zeros_like(f)
    norm2u_new = 0  # ||u_new||^2
    alpha_old = 0
    alpha_new = 0
    xi_old = 0
    xi_new = 0

    while np.sum(stopped) > 0 and n <= max_iterations - 1:
        print('iteration' + str(n))
        print(np.sum(stopped))
        # phi_i is index of phi_i in phi_List, s_i is index of s in s_List
        for [phi_i, phi] in np.stack((np.arange(0, K, step=1), phi_List), axis=1):
            for [s_i, s] in np.stack((np.arange(0, L, step=1), s_List), axis=1):
                phi_i = int(phi_i)
                s_i = int(s_i)
                # Af: radon line integral with density f, angle phi and offsetvalue s
                # len_per_pixel: length through pixels that is not zero
                # ipixels,jpixels: koordinates of non zero pixels
                Af, len_per_pixel, ipixels, jpixels = sino.radon_line(f, phi, s)
                res = Af - sinogram_p[phi_i, s_i]  # error in point phi_i, s_i
                eta_delta = eta[phi_i, s_i] + delta[phi_i, s_i]
                if abs(res) <= tau * eta_delta:
                    # discrepancy principle is fullfilled,
                    # approximation can't get better in this point
                    stopped[phi_i, s_i] = 0
                else:
                    # u_new = A(adjunct) res:
                    # calculate adjunct N**2/4 * weights * res (len_per_pixel, ipixels, jpixels, see above)
                    u_new = np.zeros_like(f)
                    u_new[ipixels, jpixels] = len_per_pixel
                    u_new *= N ** 2 / 4 * res
                    norm2u_new = discreteL2product(u_new, u_new)  # ||u_new||^2

                    # calculate alpha_new and xi_new
                    alpha_new = res * sinogram_p[phi_i, s_i]
                    xi_new = abs(res) * eta_delta

                    # norm2u_new = 0, if line integral doesn't absorb anything
                    if norm2u_new > 0:
                        # calculate first projection
                        # f_tilde = P_{H(u_new, alpha_new + xi_new)}(f)
                        f_tilde = f - abs(res) * (abs(res) - eta_delta) / norm2u_new * u_new


                        # calculate second projection
                        in_prod_u_new_u_old = discreteL2product(u_new, u_old)  # <u_new,u_old>
                        denominator_t = norm2u_new * norm2u_old - in_prod_u_new_u_old ** 2
                        if two_search_directions and norm2u_new * norm2u_old != in_prod_u_new_u_old ** 2: # no division by zero
                            # f = P_{H(u_new, alpha_new + xi_new) \cap H><(u_old, alpha_old +- xi_old)}(f_tilde)
                            # f = f_tilde + <u_new, u_old> * t * u_new - ||u_new||^2 * t * u_old
                            in_prod_f_tilde_u_old = discreteL2product(f_tilde, u_old)  # <f_tilde,u_old>
                            # check on which side of stripe f_tilde is and calculate t accordingly
                            if in_prod_f_tilde_u_old > alpha_old + xi_old:
                                t = in_prod_f_tilde_u_old - (alpha_old + xi_old)
                            elif in_prod_f_tilde_u_old < alpha_old - xi_old:
                                t = in_prod_f_tilde_u_old - (alpha_old - xi_old)
                            else:  # already on stripe, do nothing
                                t = 0
                            t /= denominator_t
                            f = f_tilde + in_prod_u_new_u_old * t * u_new - norm2u_new * t * u_old
                        else:
                            f = f_tilde
                        # set negative values to zero
                        f = np.multiply(f, f > 0)
                        # set values > 1 to 1
                        if not_bigger_1 and np.max(f) > 1:
                            cond = f > 1
                            f = ~cond * f + cond * 1
                    else:
                        print('norm_u = 0')
                    # remember old variables for next update of f
                    alpha_old = alpha_new
                    xi_old = xi_new
                    u_old = u_new
                    norm2u_old = norm2u_new
        n += 1
    return f, stopped
