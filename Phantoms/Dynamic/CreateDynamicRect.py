import numpy as np
import Dynamic


images_per_video = 450
NumOfPhantoms = 1
shift_max = 30



'''Construction of dynamic phantoms'''
i = 0
while i < NumOfPhantoms:
    remainsInUnitCircle = True
    phantom = 255 * np.load('../Images/DataRect_npy/phantom_' + str(i + 1) + '.npy')
    label = np.load('../Images/LabelsRect_npy/labels_' + str(i + 1) + '.npy')
    gif, phantom_dynamic, labels_dyn, trafo, remainsInUnitCircle = Dynamic.dynamic(images_per_video,phantom,label,shift_max)
    if remainsInUnitCircle == True:
        gif[0].save('DataRect_gif/0testphantom_dynamic_' + str(i + 1) + '.gif', save_all=True, append_images=gif[1:],
                 duration=50,
                 loop=0)
        np.save('DataDynRect_npy/0testphantom_dynamic_' + str(i + 1), phantom_dynamic)
        np.save('LabelsDynRect_npy/0testlabels_' + str(i + 1), labels_dyn)
        np.save('TrafoRect/0testmatrix_' + str(i + 1), trafo)
        i += 1
print('done')