import numpy as np
import Dynamic
import sys


images_per_video = 450
shift_max = 30

'''Construction of dynamic phantoms'''
phan_nr = int(sys.argv[1])
remainsInUnitCircle = False
while not(remainsInUnitCircle):
    phantom = 255 * np.load('../Images/DataRect_npy/phantom_' + str(phan_nr + 1) + '.npy')
    label = np.load('../Images/LabelsRect_npy/labels_' + str(phan_nr + 1) + '.npy')
    gif, phantom_dynamic, labels_dyn, trafo, remainsInUnitCircle = Dynamic.dynamic(images_per_video, phantom, label,
                                                                                   shift_max)
    if remainsInUnitCircle == True:
        gif[0].save('DataRect_gif/phantom_dynamic_' + str(phan_nr + 1) + '.gif', save_all=True, append_images=gif[1:],
                    duration=50,
                    loop=0)
        np.save('DataDynRect_npy/phantom_dynamic_' + str(phan_nr + 1), phantom_dynamic)
        np.save('LabelsDynRect_npy/labels_' + str(phan_nr + 1), labels_dyn)
        np.save('TrafoRect/matrix_' + str(phan_nr + 1), trafo)
print('done')
