import numpy as np
from PIL import Image, ImageDraw, ImageOps
import random

'''Affine transformation matrices'''


# rotate with angle alpha
def T_rotate(alpha):
    return np.array([
        [np.cos(alpha), np.sin(alpha), 0],
        [-np.sin(alpha), np.cos(alpha), 0],
        [0, 0, 1]])


# scales the object by a in first coordinate, b in second
def T_scale(a, b):
    return np.array([
        [a, 0, 0],
        [0, b, 0],
        [0, 0, 1]])


# horizontal shear by sh
def T_hshear(sh):
    return np.array([
        [1, sh, 0],
        [0, 1, 0],
        [0, 0, 1]])


# vertical shear by sv
def T_vshear(sv):
    return np.array([
        [1, 0, 0],
        [sv, 1, 0],
        [0, 0, 1]])


# translates the objects by (x,y)
def T_translate(x, y):
    return np.array([[1, 0, x],
                     [0, 1, y],
                     [0, 0, 1]])

# construct dynamic phantom
def dynamic(images_per_video, phantom, label, shift_max):
    numOfLabels = label.shape[0]
    # Prerequisities for test, that object stays in unit circle
    x = np.arange(0, phantom.shape[0])
    y = np.arange(0, phantom.shape[0])
    xv, yv = np.meshgrid(x, y, sparse=True)
    # everything inside unitcircle is false
    cond = ((xv - int((phantom.shape[0] - 2) / 2)) / int((phantom.shape[0] - 2) / 2)) ** 2 + (
            (yv - int((phantom.shape[0] - 2) / 2)) / int((phantom.shape[0] - 2) / 2)) ** 2 > 1


    labels_dyn = np.zeros((images_per_video, numOfLabels, 2), dtype='short')
    phantom_dynamic = np.zeros((images_per_video, phantom.shape[0], phantom.shape[1]))
    img = Image.fromarray(phantom)
    img = ImageOps.grayscale(img)
    gif = []
    NumOfTrafos = random.randint(2, 4)
    trafos = random.sample(range(4), k=NumOfTrafos)  # chooses NumOfTrafos different Transformations
    # random affine transformation
    if (0 in trafos):  # rotation
        rotation_total = random.uniform(-np.pi / 8, np.pi / 8)
    else:
        rotation_total = 0
    if (1 in trafos):  # shift
        shift_total = [random.randint(-shift_max, shift_max), random.randint(-shift_max, shift_max)]
    else:
        shift_total = [0, 0]
    if (2 in trafos):  # scale
        scale_total = [random.uniform(0.9, 1.1), random.uniform(0.9, 1.1)]
    else:
        scale_total = [1, 1]
    if (3 in trafos):  # shear
        shear_total = [random.uniform(-0.1, 0.1), random.uniform(-0.1, 0.1)]
    else:
        shear_total = [0, 0]

    for [alpha, shift_x, shift_y, scale_x, scale_y, h_shear, v_shear, j] in np.stack((
            np.linspace(0, rotation_total, images_per_video),
            np.linspace(0, shift_total[0], images_per_video), np.linspace(0, shift_total[1], images_per_video),
            np.linspace(1, scale_total[0], images_per_video), np.linspace(1, scale_total[1], images_per_video),
            np.linspace(0, shear_total[0], images_per_video), np.linspace(0, shear_total[1], images_per_video),
            np.arange(0, images_per_video)), axis=1):

        Trafo = T_rotate(alpha) @ T_translate(shift_x, shift_y) @ T_scale(scale_x, scale_y) @ T_vshear(
            v_shear) @ T_hshear(h_shear)
        # first translate image, s.t. (0,0) is in center, at the end translate back
        T = T_translate(int(phantom.shape[0] / 2), int(phantom.shape[0] / 2)) \
            @ Trafo \
            @ T_translate(-int(phantom.shape[0] / 2), -int(phantom.shape[0] / 2))
        T_inv = np.linalg.inv(T)
        image = img.transform(phantom.shape, Image.AFFINE, data=T_inv.flatten()[:6],
                              resample=Image.BICUBIC)
        phantom_dynamic[int(j)] = np.asarray(image) / 255
        phantom_dynamic[int(j)] = np.multiply(phantom_dynamic[int(j)], phantom_dynamic[int(j)] > 0)
        if np.sum(phantom_dynamic[int(j)] < 0) > 0:
            print('still negative values')
        labels_dyn[int(j)] = np.round(T @ np.concatenate((label, np.ones((numOfLabels, 1), dtype='short')), axis=1).T).T[:,0:2]
        # check that object remains in unit circle
        unitCircleTest = np.asarray(image) + 0  # np.asarray is read-only without +0
        unitCircleTest *= cond  # everything in unitcircle is set to zero
        if np.amax(unitCircleTest) > 0:  # object moved outside unitcircle, try new transformation
            print('object outside unitcircle ' + str(j))
            print(np.amax(unitCircleTest))
            print(trafos)
             # nothing is saved
            return gif, phantom_dynamic, labels_dyn, False
        else:
            labelImage = ImageDraw.Draw(image)
            for k in range(numOfLabels):
                labelImage.point([labels_dyn[(int(j)), k, 0], labels_dyn[(int(j), k, 1)]], fill="white")
            gif.append(image)
        '''if np.max(phantom_dynamic) > 1:
            print('Werte zu groß')
        if np.min(phantom_dynamic) < 0:
            print('Werte zu klein')'''
    return gif, phantom_dynamic, labels_dyn, Trafo, True



