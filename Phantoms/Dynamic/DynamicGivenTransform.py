import numpy as np
from PIL import Image, ImageOps

# translates the objects by (x,y)
def T_translate(x, y):
    return np.array([[1, 0, x],
                     [0, 1, y],
                     [0, 0, 1]])

# construct dynamic phantom
# The matrix A_b is a 3x3 matrix consisting of [A   b] with A 2x2 matrix and b the translation vector
#                                              [0 0 1]
def dynamic(images_per_video, phantom, A_b):
    #numOfLabels = label.shape[0]
    # Prerequisities for test, that object stays in unit circle
    x = np.arange(0, phantom.shape[0])
    y = np.arange(0, phantom.shape[0])
    xv, yv = np.meshgrid(x, y, sparse=True)
    # everything inside unitcircle is false
    cond = ((xv - int((phantom.shape[0] - 2) / 2)) / int((phantom.shape[0] - 2) / 2)) ** 2 + (
            (yv - int((phantom.shape[0] - 2) / 2)) / int((phantom.shape[0] - 2) / 2)) ** 2 > 1


    #labels_dyn = np.zeros((images_per_video, numOfLabels, 2), dtype='short')
    phantom_dynamic = np.zeros((images_per_video, phantom.shape[0], phantom.shape[1]))
    img = Image.fromarray(phantom)
    img = ImageOps.grayscale(img)
    gif = []
    # affine transformation

    for t in range(images_per_video):
        Trafo = np.eye(3) + t/(images_per_video-1) * (A_b - np.eye(3))
        # first translate image, s.t. (0,0) is in center, at the end translate back
        T = T_translate(int(phantom.shape[0] / 2), int(phantom.shape[0] / 2)) \
            @ Trafo \
            @ T_translate(-int(phantom.shape[0] / 2), -int(phantom.shape[0] / 2))
        T_inv = np.linalg.inv(T)
        image = img.transform(phantom.shape, Image.AFFINE, data=T_inv.flatten()[:6],
                              resample=Image.BICUBIC)
        phantom_dynamic[int(t)] = np.asarray(image) / 255
        phantom_dynamic[int(t)] = np.multiply(phantom_dynamic[int(t)], phantom_dynamic[int(t)] > 0)
        if np.sum(phantom_dynamic[int(t)] < 0) > 0:
            print('still negative values')
        #labels_dyn[int(j)] = np.round(T @ np.concatenate((label, np.ones((numOfLabels, 1), dtype='short')), axis=1).T).T[:,0:2]
        # check that object remains in unit circle
        unitCircleTest = np.asarray(image) + 0  # np.asarray is read-only without +0
        unitCircleTest *= cond  # everything in unitcircle is set to zero
        if np.amax(unitCircleTest) > 0:  # object moved outside unitcircle, try new transformation
            print('object outside unitcircle ' + str(t))
            print(np.amax(unitCircleTest))
             # nothing is saved
            return gif, phantom_dynamic, False
        else:
            #labelImage = ImageDraw.Draw(image)
            #for k in range(numOfLabels):
            #    labelImage.point([labels_dyn[(int(j)), k, 0], labels_dyn[(int(j), k, 1)]], fill="white")
            gif.append(image)
    return gif, phantom_dynamic, True