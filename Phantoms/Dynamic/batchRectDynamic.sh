#!/usr/bin/env bash
#SBATCH -a 251-499
#SBATCH -t 48:00:00
#SBATCH -p gpu
#SBATCH -c 16
#SBATCH -G 4


source ~/.bashrc
module load cuda
nvidia-smi
conda activate torch37
python CreateDynamicRectBash.py $SLURM_ARRAY_TASK_ID