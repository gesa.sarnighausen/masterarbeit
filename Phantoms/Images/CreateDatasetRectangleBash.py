import numpy as np
import sys
import torch
from PIL import Image

import phantoms
import matplotlib.pyplot as plt
import random

imagesize = 512
train_size = 128
# an uneven number for phantomsize is better,
# since a clear center exists.
PhantomSize = imagesize - 1

phan_nr = int(sys.argv[1])


phantom = phantoms.Phantom(PhantomSize, PhantomSize)
# rectangle
center = (random.randint(int(2 * PhantomSize / 5), int(3 * PhantomSize / 5)),
          random.randint(int(2 * PhantomSize / 5), int(3 * PhantomSize / 5)))
xLen = random.randint(5, int(PhantomSize / 10))
yLen = random.randint(5, int(PhantomSize / 10))
grayVal = random.randint(80, 255)
rect = phantoms.Rectangle(xLen, yLen, center, grayVal)
phantom.addRectangle(rect)

# convert to values between 0 and 1 (overlapping objects can lead to values above 255)
im = phantom.image.astype(np.double) / max(255, np.amax(phantom.image))
label = phantom.labels

# values outside of unit circle are set to zero
x = np.arange(0, imagesize - 1)
y = np.arange(0, imagesize - 1)
xv, yv = np.meshgrid(x, y, sparse=True)
cond = ((xv - int((imagesize - 2) / 2)) / int((imagesize - 2) / 2)) ** 2 + (
        (yv - int((imagesize - 2) / 2)) / int((imagesize - 2) / 2)) ** 2 <= 1
im *= cond
# enlarge to image size
phan = np.zeros((imagesize, imagesize))
phan[:-1, :-1] = im
plt.imshow(phan, cmap='gray', vmin=0, vmax=1)
plt.savefig('DataRect_png/phantom_' + str(phan_nr + 1))
plt.scatter(label[1:5, 0], label[1:5, 1], color='r', s=0.5)
plt.savefig('LabeledRect_png/phanLabel_' + str(phan_nr+1))
plt.close()
np.save('DataRect_npy/phantom_' + str(phan_nr + 1), phan)
np.save('LabelsRect_npy/labels_' + str(phan_nr+1), label[1:5])

im = Image.fromarray(phan)
im = im.resize((train_size, train_size))
phan = np.asarray(im, dtype=np.double)

im_pt = torch.from_numpy(phan)
im_pt = torch.unsqueeze(im_pt, 0)
torch.save(im_pt, './../../LandmarkDetectionSmallerImages/TrainingImages1'
                  '/image_' + str(phan_nr + 1) + '.pt')
label_pt = torch.from_numpy(label[1:5].astype('float64')).flatten()
label_pt = label_pt * (train_size / imagesize)
torch.save(label_pt, './../../LandmarkDetectionSmallerImages/TrainingLabels1'
                     '/labels_' + str(phan_nr + 1) + '.pt')

print('done')