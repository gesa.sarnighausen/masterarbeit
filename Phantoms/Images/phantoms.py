import numpy as np


# coordinates relate to matrix indexing

class Rectangle:
    def __init__(self, l, w, c, g):
        self.xLen = l  # scalar, only half of actual length
        self.yLen = w  # scalar, only half of actual length
        self.center = c  # coordinates as tuple
        self.grayVal = g  # in range [0,255], 0 : black, 255 : white


class Circle:
    def __init__(self, r, c, g):
        self.radius = r  # scalar
        self.center = c  # coordinates as tuple
        self.grayVal = g  # in range [0,255]


class Ellipse:
    def __init__(self, f1, f2, a, g):
        # the ellipse contains all points z in R^2 with ||z-f1|| + ||z-f2|| <= 2*a
        if 2 * a < np.sqrt((f1[0] - f2[0]) ** 2 + (f1[1] - f2[1]) ** 2):
            print('a is to small for foci1 and foci 2')
            print('a = ' + str(a))
            print('f1 = ' + str(f1))
            print('f2 = ' + str(f2))
            return
        self.foci1 = f1  # coordinates as tuple
        self.foci2 = f2  # coordinates as tuple
        self.a = a  # scalar
        self.grayVal = g  # in range [0,255]


class Phantom:
    # creates a black image of size x*y
    # with labels for a circle (1), a rectangle (4) and an ellipse (3)
    def __init__(self, x, y):
        self.xLen = x
        self.yLen = y
        self.image = np.zeros((x, y))
        self.labels = np.zeros((8, 2), dtype='short')

    def addCircle(self, circle):
        x = np.arange(0, self.xLen)
        y = np.arange(0, self.yLen)
        xv, yv = np.meshgrid(x, y, sparse=True)
        cond = (xv - circle.center[0]) ** 2 + (yv - circle.center[1]) ** 2 <= circle.radius ** 2
        self.image += circle.grayVal * cond
        self.labels[0] = circle.center  # center
        #self.labels[1] = np.array([circle.center[0], circle.center[1] + circle.radius], dtype='short')
        #self.labels[2] = np.array([circle.center[0] + circle.radius, circle.center[1]], dtype='short')
        return self

    def addRectangle(self, rectangle):
        x = np.arange(0, self.xLen)
        y = np.arange(0, self.yLen)
        xv, yv = np.meshgrid(x, y, sparse=True)
        cond1 = np.absolute(xv - rectangle.center[0]) <= rectangle.xLen
        cond2 = np.absolute(yv - rectangle.center[1]) <= rectangle.yLen
        self.image += rectangle.grayVal * cond1 * cond2
        self.labels[1] = np.array([rectangle.center[0] + rectangle.xLen, rectangle.center[1] + rectangle.yLen],
                                  dtype='short')
        self.labels[2] = np.array([rectangle.center[0] + rectangle.xLen, rectangle.center[1] - rectangle.yLen],
                                  dtype='short')
        self.labels[3] = np.array([rectangle.center[0] - rectangle.xLen, rectangle.center[1] - rectangle.yLen],
                                  dtype='short')
        self.labels[4] = np.array([rectangle.center[0] - rectangle.xLen, rectangle.center[1] + rectangle.yLen],
                                  dtype='short')
        return self

    def addEllipse(self, ellipse):
        x = np.arange(0, self.xLen)
        y = np.arange(0, self.yLen)
        xv, yv = np.meshgrid(x, y, sparse=True)
        cond = np.sqrt((xv - ellipse.foci1[0]) ** 2 + (yv - ellipse.foci1[1]) ** 2) + \
               np.sqrt((xv - ellipse.foci2[0]) ** 2 + (yv - ellipse.foci2[1]) ** 2) <= 2 * ellipse.a
        self.image += ellipse.grayVal * cond
        # calculate center, right (S1) and upper (S3) point of Ellipse
        center = np.array([(ellipse.foci1[0] + ellipse.foci2[0]) / 2, (ellipse.foci1[1] + ellipse.foci2[1]) / 2])
        S1 = center + ellipse.a * (ellipse.foci1 - center) / np.linalg.norm(ellipse.foci1 - center)
        S3 = center +np.sqrt(
            ellipse.a ** 2 - (ellipse.foci1[0] - center[0]) ** 2 - (ellipse.foci1[1] - center[1]) ** 2) * \
             (np.array([-ellipse.foci1[1] + center[1], ellipse.foci1[0] - center[0]])) / np.linalg.norm(
            ellipse.foci1 - center)
        self.labels[5] = np.round(center).astype('short')
        self.labels[6] = np.round(S1).astype('short')
        self.labels[7] = np.round(S3).astype('short')
        return self
