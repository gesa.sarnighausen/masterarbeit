#!/usr/bin/env bash
#SBATCH -a 0-499
#SBATCH -t 48:00:00
#SBATCH -p gpu
#SBATCH -c 16
#SBATCH -G 4

module purge
module load python
module load cuda

nvidia-smi
python constructPhantomsBash.py $SLURM_ARRAY_TASK_ID