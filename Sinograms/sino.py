import numpy as np
import matplotlib.pyplot as plt


# video = video as a numpyarray (NumOfImages x Imagesize_x x Imagesize_y) whose sinogram should be calculated
# NumOfOffsetvalues = number of offsetvalues used for radon transform between -1 and 1
# NumOfAngles is set to Number of Images in the video, s.t. for each image the object is scanned from a different angle
def radon_dynamic(video, NumOfOffsetvalues):
    density = video
    NumOfAngles = np.shape(density)[0]
    # list of all angles
    phi_List = np.linspace(0, np.pi, num=NumOfAngles, endpoint=False)
    # list of all offsetvalues
    s_List = np.linspace(-1, 1, num=NumOfOffsetvalues, endpoint=True)
    sinogramm = np.zeros((np.size(phi_List), np.size(s_List)))
    phi_index = 0
    s_index = 0
    for [phi, i] in np.stack((phi_List, np.arange(0, np.shape(density)[0])), axis=1):
        for s in s_List:
            # if phi_index == 220 and s_index == 113:
            sinogramm[phi_index, s_index], _, _, _ = radon_line(density[int(i)], phi, s)
            s_index += 1
        phi_index += 1
        s_index = 0
    return sinogramm


# im = image whose sinogram should be calculated
# NumOfAngles = number of angles used for radon transform between 0 and pi
# NumOfOffsetvalues = number of offsetvalues used for radon transform between -1 and 1
def radon(im, NumOfAngles, NumOfOffsetvalues):
    density = im
    # list of all angles
    phi_List = np.linspace(0, np.pi, num=NumOfAngles, endpoint=False)
    # list of all offsetvalues
    s_List = np.linspace(-1, 1, num=NumOfOffsetvalues, endpoint=True)

    sinogramm = np.zeros((np.size(phi_List), np.size(s_List)))
    # weights = np.zeros((NumOfAngles * NumOfOffsetvalues, density.shape[0] * density.shape[1]))
    phi_index = 0
    s_index = 0

    k = 0
    for phi in phi_List:
        for s in s_List:
            sinogramm[phi_index, s_index], _, _, _ = radon_line(density, phi, s)
            s_index += 1
            k += 1
        phi_index += 1
        s_index = 0
    return sinogramm


# function that calculates the line integral for the given image (density),
# a given angle (phi) and an offsetvalue (s)
def radon_line(density, phi, s):
    #assert np.min(density) >= 0.0
    #assert np.max(density) <= 1.0
    # number of pixels in x and y direction
    (x_pixel, y_pixel) = np.shape(density)

    # L(phi,x) is vertical - only intersection with x-grid
    if phi == 0:
        # the length of the line per pixel is always 2/y_pixel
        len_per_pixel = np.ones(y_pixel) * 2 / y_pixel
        # pixels in y-direction that the line goes through
        ipixels = np.arange(0, y_pixel)
        # pixels in x-direction that the line goes through
        jpixels = (1 + s) * x_pixel / 2 * np.ones(x_pixel)
        jpixels = np.floor(jpixels)

    # L(phi,x) is horizontal
    elif np.pi / 2 - 1e-3 <= phi <= np.pi / 2 + 1e-3:
        len_per_pixel = np.ones(x_pixel) * 2 / x_pixel
        ipixels = (1 - s) * y_pixel / 2 * np.ones(y_pixel)
        ipixels = np.floor(ipixels)
        jpixels = np.arange(0, x_pixel)

    else:
        # intersection of L(phi,s) with x-grid
        kx = np.arange(0, x_pixel + 1)
        # sx_tilde = array of values of gridpoints in x-direction
        sx_tilde = -1 + 2 * kx / x_pixel
        # x1 and x2 are intersection coordinates with x-grid
        x1 = s * np.cos(phi) + (s * np.sin(phi) - sx_tilde) * np.tan(phi)
        x1 = x1[:, np.newaxis]
        x2 = sx_tilde
        x2 = x2[:, np.newaxis]
        x_intersect = np.hstack((x1, x2))
        # only intersections in the grid from -1 to 1 are interesting
        x_intersect = x_intersect[np.absolute(x_intersect[:, 0]) <= 1, :]

        # intersection of L(phi,s) with y-grid
        ky = np.arange(0, y_pixel + 1)
        sy_tilde = -1 + 2 * ky / y_pixel
        y1 = sy_tilde
        y1 = y1[:, np.newaxis]
        y2 = s * np.sin(phi) + (s * np.cos(phi) - sy_tilde) / np.tan(phi)
        y2 = y2[:, np.newaxis]
        y_intersect = np.hstack((y1, y2))
        y_intersect = y_intersect[np.absolute(y_intersect[:, 1]) <= 1, :]

        # all intersections are sorted
        intersect = np.concatenate((x_intersect, y_intersect))
        s_intersect = intersect[intersect[:, 0].argsort()]
        # the length of the line per pixel is calculated with pythagoras
        len_per_pixel = np.sqrt((s_intersect[1:][:, 0] - s_intersect[0:-1][:, 0]) ** 2 + (
                s_intersect[1:][:, 1] - s_intersect[0:-1][:, 1]) ** 2)
        # calculation of midpoint between two intersections for numerical stability
        midpoints = (s_intersect[1:] + s_intersect[0:-1]) / 2
        # conversion from x and y values to i and j pixels
        jpixels = np.floor((midpoints[:, 0] + 1) * x_pixel / 2)
        ipixels = np.floor((-midpoints[:, 1] + 1) * y_pixel / 2)


    # right and bottom line belong to the last pixel
    jpixels[jpixels == x_pixel] = x_pixel - 1
    ipixels[ipixels == y_pixel] = y_pixel - 1
    ipixels = ipixels.astype(int)
    jpixels = jpixels.astype(int)

    # remove duplicate intersections
    dupli_index = np.where(len_per_pixel == 0.0)
    ipixels = np.delete(ipixels,dupli_index)
    jpixels = np.delete(jpixels, dupli_index)
    len_per_pixel = np.delete(len_per_pixel,dupli_index)


    # test for duplicate pixels
    totalpixels = np.stack((ipixels, jpixels), axis=1)
    rm_duplicates = np.unique(totalpixels, axis=0)
    if totalpixels.shape[0] != rm_duplicates.shape[0]:
        print('duplicate')
        '''plt.figure(figsize=(20, 20))
        for i in range(sy_tilde.shape[0]):
            plt.axhline(y=sy_tilde[i], color='r', linestyle='-')
            plt.axvline(x=sx_tilde[i], color='r', linestyle='-')
        for i in range(s_intersect.shape[0]):
            plt.scatter(s_intersect[i, 0], s_intersect[i, 1], marker='x', color='b')
            if i in range(midpoints.shape[0]):
                plt.scatter(midpoints[i, 0], midpoints[i, 1], marker='x', color='g')
        plt.savefig('test')
        plt.close()'''
    # sum over all pixels: density * len_per_pixel is the radon line integral
    return np.sum(density[ipixels, jpixels] * len_per_pixel), len_per_pixel, ipixels, jpixels
