import sino
import numpy as np
import matplotlib.pyplot as plt
import sys


# NumOfAngles = 450
NumOfOffsetvalues = 300

phan_nr = int(sys.argv[1])

phantom = np.load('../Phantoms/Dynamic/DataDynRect_npy/phantom_dynamic_' + str(phan_nr + 1) + '.npy')
phantom = phantom[:, :-1, :-1]
sinogram_p = sino.radon_dynamic(phantom, NumOfOffsetvalues)
sinogram = sino.radon(phantom[0], np.shape(phantom)[0], NumOfOffsetvalues)
sinogram_end = sino.radon(phantom[-1], np.shape(phantom)[0], NumOfOffsetvalues)
np.save('DataRect_p_npy/sino_p_' + str(phan_nr + 1), sinogram_p)
np.save('DataRect_npy/sino_' + str(phan_nr + 1), sinogram)
np.save('DataRect_end_npy/sino_' + str(phan_nr + 1), sinogram_end)
plt.imshow(sinogram_p, cmap='gray', vmin=0, vmax=np.max(sinogram_p))
plt.savefig('DataRect_p_png/sino_p_' + str(phan_nr + 1))
plt.close()
plt.imshow(sinogram, cmap='gray', vmin=0, vmax=np.max(sinogram))
plt.savefig('DataRect_png/sino_' + str(phan_nr + 1))
plt.close()
plt.imshow(sinogram_end, cmap='gray', vmin=0, vmax=np.max(sinogram_end))
plt.savefig('DataRect_end_png/sino_' + str(phan_nr + 1))
plt.close()
print('done')