#!/usr/bin/env bash
#SBATCH -a 251-499
#SBATCH -t 10:00
#SBATCH -p gpu
#SBATCH -c 1
#SBATCH -G 4


source ~/.bashrc
module load cuda
nvidia-smi
conda activate torch37
python CreateRectSinoBash.py $SLURM_ARRAY_TASK_ID