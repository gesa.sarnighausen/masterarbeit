import numpy as np
from matplotlib import pyplot as plt

x = np.linspace(-5,5,500)
sigmoid = 1/(1 + np.e**(-x))
tanh = np.tanh(x)
relu = x * (x > 0)
step = np.sign(x) * (x > 0)

plt.rcParams['figure.figsize'] = [16, 8]

plt.subplot(2, 2, 1)
plt.plot(x, sigmoid, c='g')
plt.title('Sigmoid')

plt.subplot(2, 2, 2)
plt.plot(x, tanh, c='g')
plt.title('Hyperbolic Tangent')

plt.subplot(2, 2, 3)
plt.plot(x, relu, c='g')
plt.title('Rectified Linear Unit (ReLU)')

plt.subplot(2, 2, 4)
plt.plot(x, step, c='g')
plt.title('Step Function')

plt.savefig('..\\LiteraturUndLatex\\Latex\\figures\\activation.png')