from sklearn.preprocessing import PolynomialFeatures
import numpy as np
from matplotlib import pyplot as plt
from sklearn.linear_model import LinearRegression
from scipy import interpolate

# correct model
x = np.linspace(-2, 2, 100)
plt.ylim(-15, 15)
data = np.array([-1.5, 0, 0.1, 0.5, 0.75, 0.8])
noise = np.random.normal(0, 0.15, size=data.shape)
data_target = -3 * data ** 2 + 5 + noise
plt.scatter(data, data_target, color='red')
plt.plot(x, -3 * x ** 2 + 5)

# linear regression
lin_reg = LinearRegression()
lin_reg.fit(data.reshape(-1, 1), data_target)
plt.plot(x.reshape(-1, 1), lin_reg.predict(x.reshape(-1, 1)), color='blue')

# polynomial regression
poly = np.poly1d(np.polynomial.polynomial.Polynomial.fit(data, data_target, 7))
test = poly(x)
plt.plot(x, test(x), color='green')

plt.legend(['quadratic regression', "linear regression", 'polynomial regression with degree 7','underlying data points'])
#plt.savefig('..\\LiteraturUndLatex\\Latex\\figures\\OverUnderfitting.png')
