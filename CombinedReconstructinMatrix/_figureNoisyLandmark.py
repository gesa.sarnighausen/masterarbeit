import numpy as np
import matplotlib.pyplot as plt
import sys
import torch

sys.path.insert(0, 'C:\\Users\\gesas\\PycharmProjects\\masterarbeit\\LandmarkDetectionSmallerImages')
import CNN6 as net


params_name = '../LandmarkDetectionSmallerImages/2pretrain_params.pt'
# settings for pytorch
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
torch.cuda.empty_cache()
torch.set_default_dtype(torch.float64)
model = net.CNN7()
model.to(device)
model.load_state_dict(torch.load(params_name,map_location=torch.device('cpu')))
model.eval()

rec_start = np.load('noisytestResKac_start_128.npy')
rec_end = np.load('noisytestResKac_end_128.npy')

image_start = torch.from_numpy(rec_start)
image_start = torch.unsqueeze(image_start, 0)
image_end = torch.from_numpy(rec_end)
image_end = torch.unsqueeze(image_end, 0)
landmarks_end = model(torch.unsqueeze(image_end, 0).to(device)).detach().numpy()
EndCorners = np.resize(landmarks_end, (4,2))
landmarks_start = model(torch.unsqueeze(image_start, 0).to(device)).detach().numpy()
StartCorners = np.resize(landmarks_start, (4,2))

#compute detected shift
shift = EndCorners - StartCorners
shift_mean = np.mean((shift), axis=0)
shift_mean_corrected = shift_mean * 512/128
shift_true = np.array([51,51])
error = np.absolute(shift_true - shift_mean_corrected)/51

#plot detected landmarks
figure, axis = plt.subplots(1, 2)
axis[0].imshow(rec_start, cmap='gray', vmin=0, vmax=1)
axis[0].scatter(StartCorners[:, 0], StartCorners[:, 1], color='red', s=0.2)
axis[1].imshow(rec_end, cmap='gray', vmin=0, vmax=1)
axis[1].scatter(EndCorners[:, 0], EndCorners[:, 1], color='red', s=0.2)
axis[0].set_title('t=0')
axis[1].set_title('t=449')
plt.savefig('figureNoiseLandmark.png')
plt.show()