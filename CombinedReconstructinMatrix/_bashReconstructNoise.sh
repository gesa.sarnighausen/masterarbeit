#!/usr/bin/env bash
#SBATCH -t 120:00:00
#SBATCH -p gpu
#SBATCH -q long
#SBATCH -c 16
#SBATCH -G 4

source ~/.bashrc

module load cuda
nvidia-smi

conda activate torch37
python _reconstruct.py 'noise'