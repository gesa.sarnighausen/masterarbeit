import numpy as np
import scipy
import matplotlib.pyplot as plt
import sys
import torch
import time

sys.path.insert(0, 'C:\\Users\\gesas\\PycharmProjects\\masterarbeit\\KaczmarzRESESOP_Reconstructions')
sys.path.insert(0, '/home/uni08/gsarnig/masterarbeit/KaczmarzRESESOP_Reconstructions')
import Resesop

sys.path.insert(0, 'C:\\Users\\gesas\\PycharmProjects\\masterarbeit\\FilteredBackprojection')
sys.path.insert(0, '/home/uni08/gsarnig/masterarbeit/FilteredBackprojection')
import dynamicBackprojection as dynFBP

sys.path.insert(0, 'C:\\Users\\gesas\\PycharmProjects\\masterarbeit\\LandmarkDetectionSmallerImages')
sys.path.insert(0, '/home/uni08/gsarnig/masterarbeit/LandmarkDetectionSmallerImages')
import CNN6 as net
import CERDataset

test = scipy.constants.pi
resesop_size = 128
phantom_size = 512
rec_size = phantom_size - int(phantom_size / 20)

mode = sys.argv[1]
print(mode)


if mode == 'noise':
    sinogram_start = np.load('noisytestsinostart_512.npy')
    sinogram_end = np.load('noisytestsinoend_512.npy')
    sinogram_dyn = np.load('noisytestsinodynamic_512.npy')
    noise = np.load('noise.npy')
else:
    sinogram_start = np.load('testsinostart_512.npy')
    sinogram_end = np.load('testsinoend_512.npy')
    sinogram_dyn = np.load('testsinodynamic_512.npy')

# reconstruct with Resesop-Kaczmarz

if mode == 'noise':
    start_time = time.time()
    eta_start = np.absolute(sinogram_start - sinogram_dyn)
    eta_end = np.absolute(sinogram_end - sinogram_dyn)
    delta = noise
    rec_start, _ = Resesop.run_resesop(sinogram_dyn, eta_start, delta,
                                       reconstruction_size=resesop_size, max_iterations=3)
    rec_end, _ = Resesop.run_resesop(sinogram_dyn, eta_end, delta,
                                     reconstruction_size=resesop_size, max_iterations=3)
    end_time = time.time()
    resesopHelp_time = end_time - start_time

    f = open('timeNoisy.txt', "a")
    f.write('resesop for landmarkdetection:' + str(resesopHelp_time) + '\n')
    f.close()

    np.save('noisytestResKac_start_' + str(resesop_size) + '.npy', rec_start)
    plt.imshow(rec_start, cmap='gray', vmin=0, vmax=1)
    plt.savefig('noisytestResKac_start_' + str(resesop_size) + '.png')
    plt.close()
    np.save('noisytestResKac_end_' + str(resesop_size) + '.npy', rec_end)
    plt.imshow(rec_end, cmap='gray', vmin=0, vmax=1)
    plt.savefig('noisytestResKac_end_' + str(resesop_size) + '.png')
    plt.close()

else:
    start_time = time.time()
    eta_start = np.absolute(sinogram_start - sinogram_dyn)
    eta_end = np.absolute(sinogram_end - sinogram_dyn)
    delta = np.zeros_like(sinogram_dyn)
    '''rec_start, _ = Resesop.run_resesop(sinogram_dyn, eta_start, delta,
                                       reconstruction_size=resesop_size, max_iterations=3)
    rec_end, _ = Resesop.run_resesop(sinogram_dyn, eta_end, delta,
                                     reconstruction_size=resesop_size, max_iterations=3)'''
    end_time = time.time()
    resesopHelp_time = end_time - start_time

    f = open('time.txt', "a")
    f.write('resesop for landmarkdetection:' + str(resesopHelp_time) + '\n')
    f.close()

    '''np.save('testResKac_start_' + str(resesop_size) + '.npy', rec_start)
    plt.imshow(rec_start, cmap='gray', vmin=0, vmax=1)
    plt.savefig('testResKac_start_' + str(resesop_size) + '.png')
    plt.close()
    np.save('testResKac_end_' + str(resesop_size) + '.npy', rec_end)
    plt.imshow(rec_end, cmap='gray', vmin=0, vmax=1)
    plt.savefig('testResKac_end_' + str(resesop_size) + '.png')
    plt.close()'''

start_time = time.time()
rec_resesop, _ = Resesop.run_resesop(sinogram_dyn, eta_start, delta,
                                   reconstruction_size=rec_size, max_iterations=30)
end_time = time.time()
resesop_time = end_time - start_time

if mode == 'noise':
    f = open('timeNoisy.txt', "a")
    f.write('resesop 30 iterations:' + str(resesop_time) + '\n')
    f.close()

    np.save('noisytestResKac30iter.npy', rec_resesop)
    plt.imshow(rec_resesop, cmap='gray', vmin=0, vmax=1)
    plt.savefig('noisytestResKac30iter.png')
    plt.close()


else:
    f = open('time.txt', "a")
    f.write('resesop 30 iterations:' + str(resesop_time) + '\n')
    f.close()

    np.save('testResKac30iter.npy', rec_resesop)
    plt.imshow(rec_resesop, cmap='gray', vmin=0, vmax=1)
    plt.savefig('testResKac30iter.png')
    plt.close()

if mode == 'noise':
    rec_start = np.load('noisytestResKac_start_128.npy')
    rec_end = np.load('noisytestResKac_end_128.npy')
else:
    rec_start = np.load('testResKac_start_128.npy')
    rec_end = np.load('testResKac_end_128.npy')

# Landmarkdetection

start_time = time.time()
params_name = '../LandmarkDetectionSmallerImages/2pretrain_params.pt'
# settings for pytorch
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
torch.cuda.empty_cache()
torch.set_default_dtype(torch.float64)
model = net.CNN7()
model.to(device)
model.load_state_dict(torch.load(params_name,map_location=torch.device('cuda')))
#model.load_state_dict(torch.load(params_name,map_location=torch.device('cpu')))
model.eval()

image_start = torch.from_numpy(rec_start)
image_start = torch.unsqueeze(image_start, 0)
image_end = torch.from_numpy(rec_end)
image_end = torch.unsqueeze(image_end, 0)
landmarks_end = model(torch.unsqueeze(image_end, 0).to(device)).cpu().detach().numpy()

EndCorners = np.resize(landmarks_end, (4,2))
landmarks_start = model(torch.unsqueeze(image_start, 0).to(device)).cpu().detach().numpy()
StartCorners = np.resize(landmarks_start, (4,2))

# correct landmark detection
EndCorners_corr = EndCorners.copy()
EndCorners_corr[0,1] = StartCorners[0,1]
EndCorners_corr[3,1] = StartCorners[3,1]
#convert pixels to coordinates
delta = 2/128
StartCorners_coord = np.zeros_like(StartCorners)
EndCorners_coord = np.zeros_like(EndCorners)
EndCorners_coord_corr = np.zeros_like(EndCorners)
for ind in range(4):
    StartCorners_coord[ind,0] = 1 - delta * (StartCorners[ind,0] + 1 / 2)
    StartCorners_coord[ind,1] = -1 + delta * (StartCorners[ind,1] + 1 / 2)
    EndCorners_coord_corr[ind,0] = 1 - delta * (EndCorners_corr[ind,0] + 1 / 2)
    EndCorners_coord_corr[ind,1] = -1 + delta * (EndCorners_corr[ind,1] + 1 / 2)
    EndCorners_coord[ind,0] = 1 - delta * (EndCorners[ind,0] + 1 / 2)
    EndCorners_coord[ind,1] = -1 + delta * (EndCorners[ind,1] + 1 / 2)

plt.xlim([-1,1])
plt.ylim([-1,1])
plt.scatter(StartCorners_coord[:, 1], StartCorners_coord[:, 0], color='red', s=1)
plt.scatter(EndCorners_coord[:, 1], EndCorners_coord[:, 0], color='blue', s=1)
plt.show()
# build matrix for least square solution
A = np.zeros((8,4))
A[0,0] = StartCorners_coord[0,0]
A[0,1] = StartCorners_coord[0,1]
A[1,2] = StartCorners_coord[0,0]
A[1,3] = StartCorners_coord[0,1]

A[2,0] = StartCorners_coord[1,0]
A[2,1] = StartCorners_coord[1,1]
A[3,2] = StartCorners_coord[1,0]
A[3,3] = StartCorners_coord[1,1]

A[4,0] = StartCorners_coord[2,0]
A[4,1] = StartCorners_coord[2,1]
A[5,2] = StartCorners_coord[2,0]
A[5,3] = StartCorners_coord[2,1]

A[6,0] = StartCorners_coord[3,0]
A[6,1] = StartCorners_coord[3,1]
A[7,2] = StartCorners_coord[3,0]
A[7,3] = StartCorners_coord[3,1]


lstsol_corr = np.linalg.lstsq(A,np.resize(EndCorners_coord_corr, (8,1)))
lstsol = np.linalg.lstsq(A,np.resize(EndCorners_coord, (8,1)))
matrixmov_corr = lstsol_corr[0].reshape([2,2])
matrixmov = lstsol[0].reshape([2,2])
end_time = time.time()
time_calcMat = start_time - end_time

#plot detected landmarks
figure, axis = plt.subplots(1, 2)

axis[0].imshow(rec_start, cmap='gray', vmin=0, vmax=1)
axis[0].scatter(StartCorners[:, 0], StartCorners[:, 1], color='red', s=0.2)
axis[1].imshow(rec_end, cmap='gray', vmin=0, vmax=1)
axis[1].scatter(EndCorners_corr[:, 0], EndCorners_corr[:, 1], color='red', s=0.2)
axis[0].set_title('Start')
axis[1].set_title('End')
plt.show()




def b_0(t, images_per_video):
    return np.zeros(2)

def A_comp(t, images_per_video):
    return np.linalg.inv(np.eye(2) + t/(images_per_video-1) * (matrixmov - np.eye(2)))

def A_corr(t, images_per_video):
    return np.linalg.inv(np.eye(2) + t/(images_per_video-1) * (matrixmov_corr - np.eye(2)))

def A(t, images_per_video):
    return np.linalg.inv(np.eye(2) + t/(images_per_video-1) * (np.array([[2, 0], [0, 1]]) - np.eye(2)))

def I(t, images_per_video):
    return np.eye(2)

if mode == 'noise':
    sino = np.load('noisytestsinodynamic_' + str(phantom_size) + '.npy')
else:
    sino = np.load('testsinodynamic_' + str(phantom_size) + '.npy')

print('starting first reconstruction')
rec = dynFBP.FBP_dynamic(sino, rec_size, A_comp, dynFBP.derivative_A_inv_T_times_theta, b_0, dynFBP.gaussian_kernel)
if mode == 'noise':
    np.save('noisytestComputed' + str(phantom_size) +'.npy', rec)
else:
    np.save('testComputed' + str(phantom_size) + '.npy', rec)

print('starting 2nd reconstruction')
rec = dynFBP.FBP_dynamic(sino, rec_size, A_corr, dynFBP.derivative_A_inv_T_times_theta, b_0, dynFBP.gaussian_kernel)
if mode == 'noise':
    np.save('noisytestCorrected' + str(phantom_size) +'.npy', rec)
else:
    np.save('testCorrected' + str(phantom_size) + '.npy', rec)


print('starting 3rd reconstruction')
rec = dynFBP.FBP_dynamic(sino, rec_size, I, dynFBP.derivative_A_inv_T_times_theta, b_0, dynFBP.gaussian_kernel)
if mode == 'noise':
    np.save('noisytestNoMovement' + str(phantom_size) +'.npy', rec)
else:
    np.save('testNoMovement' + str(phantom_size) +'.npy', rec)




print('done')
