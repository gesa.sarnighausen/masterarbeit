import numpy as np
import matplotlib.pyplot as plt
from PIL import Image, ImageOps
import math
import skimage.metrics as err

dynFBP_comp = np.load('testComputed512.npy')
dynFBP_real = np.load('testRealMovement512png.npy')
dynFBP_corr = np.load('testCorrected512.npy')
fbp = np.load('testNoMovement512.npy')
phan = np.load('testphantom_512.npy')

phan[phan<0] = 0
dynFBP_comp[dynFBP_comp < 0] = 0
dynFBP_real[dynFBP_real < 0] = 0
dynFBP_corr[dynFBP_corr < 0] = 0
fbp[fbp < 0] = 0

im = Image.fromarray(255*phan)
im = ImageOps.grayscale(im)
im = im.resize((dynFBP_corr.shape[0], dynFBP_corr.shape[1]))
phan = np.asarray(im)
phan = phan/255

# calculate error
# relative l2 error

l2DynFBP = np.linalg.norm(dynFBP_real - phan) / np.linalg.norm(phan)


# MSE

mseDynFBP = err.mean_squared_error(phan, dynFBP_real)


# PSNR

psnrDynFBP = err.peak_signal_noise_ratio(phan, dynFBP_real)


# SSIM

ssimDynFBP = err.structural_similarity(phan, dynFBP_real)


f = open('error.txt', "a")
f.write('l2DynFBPRealMovement: ' + str(l2DynFBP) + '\n')
f.write('mseDynFBPRealMovement: ' + str(mseDynFBP) + '\n')
f.write('psnrDynFBPRealMovement: ' + str(psnrDynFBP) + '\n')
f.write('ssimDynFBPRealMovement: ' + str(ssimDynFBP) + '\n')
f.close()



figure, axis = plt.subplots(2, 2, figsize=(7,7))

axis[0][0].imshow(dynFBP_real, cmap='gray', vmin=0, vmax=1)
axis[0][1].imshow(dynFBP_corr, cmap='gray', vmin=0, vmax=1)
axis[1][0].imshow(dynFBP_comp, cmap='gray', vmin=0, vmax=1)
axis[1][1].imshow(fbp, cmap='gray',vmin=0,vmax=1)
axis[0][0].set_title('Real')
axis[0][1].set_title('Corrected')
axis[1][0].set_title('Computed')
axis[1][1].set_title('static FBP')
plt.savefig('figureComparisonFBP.png')
plt.show()
print('done')