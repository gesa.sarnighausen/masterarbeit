import numpy as np
import matplotlib.pyplot as plt

phan_dyn = np.load('testDynamic_512.npy')

figure, axis = plt.subplots(1, 2)
axis[0].imshow(phan_dyn[0], cmap='gray', vmin=0, vmax=1)
axis[1].imshow(phan_dyn[-1], cmap='gray', vmin=0, vmax=1)
axis[0].set_title('t=0')
axis[1].set_title('t=449')
axis[0].set_axis_off()
axis[1].set_axis_off()
plt.savefig('figureShift.png')
plt.show()