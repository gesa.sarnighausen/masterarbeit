import numpy as np
import matplotlib.pyplot as plt

sino_dyn = np.load('testsinodynamic_512.npy')
sino_s = np.load('testsinostart_512.npy')
sino_e = np.load('testsinoend_512.npy')

figure, axis = plt.subplots(1, 3, figsize=(6,3), dpi=500)
axis[0].imshow(sino_dyn, cmap='gray', vmin=0, vmax=np.max(sino_dyn))
axis[1].imshow(sino_s, cmap='gray', vmin=0, vmax=np.max(sino_s))
axis[2].imshow(sino_e, cmap='gray', vmin=0, vmax=np.max(sino_e))
size = 13
axis[0].set_title('Dynamic Sinogram',fontsize=size)
axis[1].set_title('Sinogram Start',fontsize=size)
axis[2].set_title('Sinogram End',fontsize=size)
axis[0].set_axis_off()
axis[1].set_axis_off()
axis[2].set_axis_off()
plt.subplots_adjust(left=0, right=1, top=0.9, bottom=0)
plt.savefig('figureSinos.png')
plt.show()