import numpy as np
import matplotlib.pyplot as plt
from PIL import Image, ImageOps
import math
import skimage.metrics as err

fbp = np.load('testFinalRecNoMotion.npy')
dynFBP = np.load('testFinalRec.npy')
resesop = np.load('testResKac30iter.npy')
phan = np.load('testphantom_512.npy')

phan[phan<0] = 0
fbp[fbp < 0] = 0
dynFBP[dynFBP < 0] = 0
resesop[resesop < 0] = 0

im = Image.fromarray(255*phan)
im = ImageOps.grayscale(im)
im = im.resize((resesop.shape[0],resesop.shape[1]))
phan = np.asarray(im)
phan = phan/255

# calculate error
# relative l2 error
'''l2FBP = np.linalg.norm(fbp - phan)/np.linalg.norm(phan)
l2DynFBP = np.linalg.norm(dynFBP - phan)/np.linalg.norm(phan)
l2resesop = np.linalg.norm(resesop - phan)/np.linalg.norm(phan)

# MSE
mseFBP = err.mean_squared_error(phan,fbp)
mseDynFBP = err.mean_squared_error(phan,dynFBP)
mseResesop = err.mean_squared_error(phan,resesop)

# PSNR
psnrFBP = err.peak_signal_noise_ratio(phan,fbp)
psnrDynFBP = err.peak_signal_noise_ratio(phan, dynFBP)
psnrResesop = err.peak_signal_noise_ratio(phan, resesop)

# SSIM
ssimFBP = err.structural_similarity(phan,fbp)
ssimDynFBP = err.structural_similarity(phan,dynFBP)
ssimResesop = err.structural_similarity(phan,resesop)

f = open('error.txt', "a")
f.write('l2FBP: ' + str(l2FBP) + '\n')
f.write('l2DynFBP: ' + str(l2DynFBP) + '\n')
f.write('l2resesop: ' + str(l2resesop) + '\n')
f.write('mseFBP: ' + str(mseFBP) + '\n')
f.write('mseDynFBP: ' + str(mseDynFBP) + '\n')
f.write('mseresesop: ' + str(mseResesop) + '\n')
f.write('psnrFBP: ' + str(psnrFBP) + '\n')
f.write('psnrDynFBP: ' + str(psnrDynFBP) + '\n')
f.write('psnrResesop: ' + str(psnrResesop) + '\n')
f.write('ssimFBP: ' + str(ssimFBP) + '\n')
f.write('ssimDynFBP: ' + str(ssimDynFBP) + '\n')
f.write('ssimResesop: ' + str(ssimResesop) + '\n')
f.close()'''



figure, axis = plt.subplots(1, 3, figsize=(6,2), dpi=512)

axis[0].imshow(dynFBP, cmap='gray', vmin=0, vmax=1)
axis[1].imshow(resesop, cmap='gray', vmin=0, vmax=1)
axis[2].imshow(fbp, cmap='gray', vmin=0, vmax=1)
size = 9.5
axis[0].set_title('Hybrid Method',fontsize=size)
axis[1].set_title('RESESOP-Kaczmarz',fontsize=size)
axis[2].set_title('Static FBP',fontsize=size)
axis[0].set_axis_off()
axis[1].set_axis_off()
axis[2].set_axis_off()
plt.subplots_adjust(left=0, right=1, top=0.9, bottom=0)
plt.savefig('figureComparisonShift.png')
plt.show()


