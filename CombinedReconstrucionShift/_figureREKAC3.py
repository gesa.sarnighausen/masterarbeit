import numpy as np
import matplotlib.pyplot as plt

rec_s = np.load('testResKac_start_128.npy')
rec_e = np.load('testResKac_end_128.npy')

figure, axis = plt.subplots(1, 2, figsize=(6,3), dpi=812)
axis[0].imshow(rec_s, cmap='gray', vmin=0, vmax=1)
axis[1].imshow(rec_e, cmap='gray', vmin=0, vmax=1)

axis[0].set_axis_off()
axis[1].set_axis_off()
size = 30
axis[0].set_title('start time',fontsize=size)
axis[1].set_title('end time',fontsize=size)
plt.subplots_adjust(left=0, right=1, top=0.85, bottom=0)
plt.savefig('figureREKAC3.png')
plt.show()
