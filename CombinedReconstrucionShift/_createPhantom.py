import numpy as np
import matplotlib.pyplot as plt
import sys
import time


sys.path.insert(0, 'C:\\Users\\gesas\\PycharmProjects\\masterarbeit\\Sinograms')
sys.path.insert(0, '/home/uni08/gsarnig/masterarbeit/Sinograms')

import sino

sys.path.insert(0, 'C:\\Users\\gesas\\PycharmProjects\\masterarbeit\\Phantoms\\Dynamic')
sys.path.insert(0, '/home/uni08/gsarnig/masterarbeit/Phantoms/Dynamic')
import DynamicGivenTransform



phantom_size = 512
NumOfOffsetValues = 300
NumOfAngles = 450

mode = sys.argv[1]
print(mode)

# create static testphantom
phantom = np.zeros((phantom_size, phantom_size))
phantom[175:275, 200:250] = 0.6


plt.imshow(phantom, cmap='gray', vmin=0, vmax=1)
plt.savefig('testphantom_' + str(phantom_size) + '.png')
plt.close()
np.save('testphantom_' + str(phantom_size) + '.npy', phantom)

# create dynamic test phantom with shift
A_b = np.array([[1, 0, int(phantom_size / 10)], [0, 1, int(phantom_size / 10)], [0, 0, 1]])
gif, phantom_dynamic, notunitcircle = DynamicGivenTransform.dynamic(images_per_video=NumOfAngles, phantom=255 * phantom,
                                                                    A_b=A_b)

gif[0].save('testDynamic_' + str(phantom_size) + '.gif', save_all=True, append_images=gif[1:], duration=50, loop=0)
np.save('testDynamic_' + str(phantom_size) + '.npy', phantom_dynamic)

# create sinos
start_time = time.time()
sinogram_dyn = sino.radon_dynamic(phantom_dynamic, NumOfOffsetvalues=NumOfOffsetValues)
end_time = time.time()
time_dynsino = end_time - start_time
start_time = time.time()
sinogram_start = sino.radon(phantom_dynamic[0], NumOfAngles=NumOfAngles, NumOfOffsetvalues=NumOfOffsetValues)
end_time = time.time()
time_sinostart = end_time - start_time
start_time = time.time()
sinogram_end = sino.radon(phantom_dynamic[-1], NumOfAngles=NumOfAngles, NumOfOffsetvalues=NumOfOffsetValues)
end_time = time.time()
time_sinoend = end_time - start_time



if mode == 'noise':
    f = open('timeNoisy.txt', "a")
    f.write('dynamic sino:' + str(time_dynsino) + '\n')
    f.write('start sino:' + str(time_sinostart) + '\n')
    f.write('end sino:' + str(time_sinoend) + '\n')
    f.close()

    noise = np.random.uniform(-0.02, 0.02, (sinogram_dyn.shape[0], sinogram_dyn.shape[1]))
    np.save('noise.npy', noise)

    sinogram_dyn += noise
    np.save('noisytestsinodynamic_' + str(phantom_size) + '.npy', sinogram_dyn)
    plt.imshow(sinogram_dyn, cmap='gray', vmin=0, vmax=1)
    plt.savefig('noisytestsinodynamic_' + str(phantom_size) + '.png')
    plt.close()

    sinogram_start += noise
    np.save('noisytestsinostart_' + str(phantom_size) + '.npy', sinogram_start)
    plt.imshow(sinogram_start, cmap='gray', vmin=0, vmax=1)
    plt.savefig('noisytestsinostart_' + str(phantom_size) + '.png')
    plt.close()

    sinogram_end += noise
    np.save('noisytestsinoend_' + str(phantom_size) + '.npy', sinogram_end)
    plt.imshow(sinogram_end, cmap='gray', vmin=0, vmax=1)
    plt.savefig('noisytestsinoend_' + str(phantom_size) + '.png')
    plt.close()
else:
    f = open('time.txt', "a")
    f.write('dynamic sino:' + str(time_dynsino) + '\n')
    f.write('start sino:' + str(time_sinostart) + '\n')
    f.write('end sino:' + str(time_sinoend) + '\n')
    f.close()

    np.save('testsinodynamic_' + str(phantom_size) + '.npy', sinogram_dyn)
    plt.imshow(sinogram_dyn, cmap='gray', vmin=0, vmax=1)
    plt.savefig('testsinodynamic_' + str(phantom_size) + '.png')
    plt.close()

    np.save('testsinostart_' + str(phantom_size) + '.npy', sinogram_start)
    plt.imshow(sinogram_start, cmap='gray', vmin=0, vmax=1)
    plt.savefig('testsinostart_' + str(phantom_size) + '.png')
    plt.close()

    np.save('testsinoend_' + str(phantom_size) + '.npy', sinogram_end)
    plt.imshow(sinogram_end, cmap='gray', vmin=0, vmax=1)
    plt.savefig('testsinoend_' + str(phantom_size) + '.png')
    plt.close()

print('done')