import numpy as np
import matplotlib.pyplot as plt
from PIL import Image, ImageOps

rec = np.load('noisytestFinalRec.npy')
phan = np.load('testphantom_512.npy')

im = Image.fromarray(255*phan)
im = ImageOps.grayscale(im)
im = im.resize((rec.shape[0],rec.shape[1]))
phan = np.asarray(im)
phan = phan/255


figure, axis = plt.subplots(1, 2)
axis[0].imshow(phan, cmap='gray', vmin=0, vmax=1)
axis[1].imshow(rec, cmap='gray', vmin=0, vmax=1)
axis[0].set_title('Original Phantom')
axis[1].set_title('Reconstruction')
plt.savefig('figureNoisyDynFBP.png')
plt.show()