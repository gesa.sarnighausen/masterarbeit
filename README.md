# Masterarbeit



## Phantoms

The directory 'Phantoms' contains the directories 'Images' and 'Dynamic' and is used to build a dataset of phantoms. First static phantoms are generated in the 'Images' directory, then the dynamic ones in the 'Dynamic' directory. The .sh files can be executed on the gwdg cluster as slurm jobs.

## Sinograms

After the phantoms are generated, sinograms are generated. DataRect_p are the dynamic sinograms, DataRect the static sinograms of the start point and DataRect_end the static sinograms of the end point.
The file sino.py includes functions to compute the static and dynamich Radon transform

## KaczmarzRESESOP_Reconstructions

Resesop.py includes the RESESOP-Kaczmarz algorithm. In this directory reconstructions of the first and last time step for the data set are made for the landmark detection.

## LandmarkDetection

CreateDatasetRectangle.py creates the pretraining dataset. The dataset of reconstructions was created by KaczmarzRESESOP_Reconstructions. Training.py performs the training. If the mode is 'pretrain1' the pretraining dataset is used, if it is 'pretrain2' the training dataset is used.
evaluateModel.py evaluates the model for the two modes and gives back the loss graphic and performs landmark detection on a few random images from the validation set.

## CombinedReconstructionShift, CombinedReconstructionMatrix and RealData

All three directories are similar. First the data is prepared in _createPhantom.py or _prepareData.py. Then the combined method is performed in the file _reconstruct.py. In the simulated cases, all files work with mode 'noise' or else (noiseless).

