import numpy as np
import matplotlib.pyplot as plt
import skimage.metrics as err
from PIL import Image, ImageOps
from scipy.io import savemat

fbp = np.load('recFBP.npy')
dynFBP = np.load('recDynFBP.npy')
resesop = np.load('recResKac30iter.npy')
phan = np.load('myGroundtruth.npy')



'''phan[phan<0] = 0
fbp[fbp < 0] = 0
dynFBP[dynFBP < 0] = 0
resesop[resesop < 0] = 0'''

# save to matlab
'''mdic = {"NoiseFbp": fbp, "label": "im"}
savemat('NoiseFbp.mat',mdic)
mdic = {"NoiseDynFbp": dynFBP, "label": "im"}
savemat('NoiseDynFbp.mat',mdic)
mdic = {"NoiseResesop": resesop, "label": "im"}
savemat('NoiseResesop.mat',mdic)

im = Image.fromarray(255*phan)
im = ImageOps.grayscale(im)
im = im.resize((resesop.shape[0],resesop.shape[1]))
phan = np.asarray(im)
phan = phan/255

mdic = {"phan": phan, "label": "im"}
savemat('phan.mat',mdic)'''

# calculate error
# relative l2 error
'''l2FBP = np.linalg.norm(fbp - phan)/np.linalg.norm(phan)
l2DynFBP = np.linalg.norm(dynFBP - phan)/np.linalg.norm(phan)
l2resesop = np.linalg.norm(resesop - phan)/np.linalg.norm(phan)

# MSE
mseFBP = err.mean_squared_error(phan,fbp)
mseDynFBP = err.mean_squared_error(phan,dynFBP)
mseResesop = err.mean_squared_error(phan,resesop)

# PSNR
psnrFBP = err.peak_signal_noise_ratio(phan,fbp)
psnrDynFBP = err.peak_signal_noise_ratio(phan, dynFBP)
psnrResesop = err.peak_signal_noise_ratio(phan, resesop)

# SSIM
ssimFBP = err.structural_similarity(phan,fbp)
ssimDynFBP = err.structural_similarity(phan,dynFBP)
ssimResesop = err.structural_similarity(phan,resesop)
meanFBP = np.mean(fbp.flatten())
meanDynFBP = np.mean(dynFBP.flatten())
meanREsesop = np.mean(resesop.flatten())
meanPhan = np.mean(phan.flatten())
N = np.size(fbp.flatten())
covFBP = 1/(N-1)*np.sum((phan-meanPhan)*(fbp - meanFBP))
covDynFBP = 1/(N-1)*np.sum((phan-meanPhan)*(dynFBP - meanDynFBP))
covREsesop =1/(N-1)*np.sum((phan-meanPhan)*(resesop - meanREsesop))'''

'''f = open('error.txt', "a")
f.write('l2FBP: ' + str(l2FBP) + '\n')
f.write('l2DynFBP: ' + str(l2DynFBP) + '\n')
f.write('l2resesop: ' + str(l2resesop) + '\n')
f.write('mseFBP: ' + str(mseFBP) + '\n')
f.write('mseDynFBP: ' + str(mseDynFBP) + '\n')
f.write('mseresesop: ' + str(mseResesop) + '\n')
f.write('psnrFBP: ' + str(psnrFBP) + '\n')
f.write('psnrDynFBP: ' + str(psnrDynFBP) + '\n')
f.write('psnrResesop: ' + str(psnrResesop) + '\n')
f.write('ssimFBP: ' + str(ssimFBP) + '\n')
f.write('ssimDynFBP: ' + str(ssimDynFBP) + '\n')
f.write('ssimResesop: ' + str(ssimResesop) + '\n')
f.close()
'''

figure, axis = plt.subplots(2,2, figsize=(2.75,2.75), dpi=512)

axis[0][0].imshow(dynFBP, cmap='gray', vmin=0)
axis[0][1].imshow(resesop, cmap='gray', vmin=0)
axis[1][0].imshow(fbp, cmap='gray', vmin=0)
axis[1][1].imshow(phan, cmap='gray', vmin=0)
size = 7
axis[0][0].set_title('Hybrid Method',fontsize=size)
axis[0][1].set_title('RESESOP-Kaczmarz',fontsize=size)
axis[1][0].set_title('Static FBP',fontsize=size)
axis[1][1].set_title('FBP reference',fontsize=size)
axis[0][0].set_axis_off()
axis[0][1].set_axis_off()
axis[1][0].set_axis_off()
axis[1][1].set_axis_off()
plt.subplots_adjust(left=0, right=1, top=0.94, bottom=0)
figure.savefig('figureComparisonLego.png')
plt.show()