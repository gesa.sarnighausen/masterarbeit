import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
import sys
import torch



rec_start = np.load('recResKac_start_128.npy')
rec_end = np.load('recResKac_end_128.npy')

# Landmarkdetection by hand
# top left, top right, bottom left, bottom right [i,j]
StartCorners = np.array([[12, 77], [31,104],[53,51], [71,76]])
EndCorners = np.array([[53,51], [70,77],[93,25], [111,49]])

shift = np.mean((EndCorners - StartCorners), axis=0)


#plot detected landmarks
'''figure, axis = plt.subplots(1, 3)
axis[0].imshow(rec_start, cmap='gray')
axis[0].scatter(StartCorners[:, 1], StartCorners[:, 0], color='red', s=5, marker='X')
axis[1].imshow(rec_end, cmap='gray')
axis[1].scatter(EndCorners[:, 1], EndCorners[:, 0], color='red', s=5, marker='X')
axis[0].set_title('t=1')
axis[1].set_title('t=4')
axis[0].set_axis_off()
axis[1].set_axis_off()
plt.savefig('figureLandmark.png')
plt.show()'''

#plot detected landmarks + lego image


lego = Image.open('lego.jpg')
lego_np = np.asarray(lego)


figure, axis = plt.subplots(1, 3, figsize=(6,2), dpi=512)

axis[0].imshow(lego_np)
axis[1].imshow(rec_start, cmap='gray')
axis[1].scatter(StartCorners[:, 1], StartCorners[:, 0], color='red', s=5, marker='X')
axis[2].imshow(rec_end, cmap='gray')
axis[2].scatter(EndCorners[:, 1], EndCorners[:, 0], color='red', s=5, marker='X')
size = 9.5
axis[1].set_title('t = 0',fontsize=size)
axis[2].set_title('t = 3',fontsize=size)
axis[0].set_title('lego brick at t = 0',fontsize=size)
axis[0].set_axis_off()
axis[1].set_axis_off()
axis[2].set_axis_off()
plt.subplots_adjust(left=0, right=1, top=0.9, bottom=0)
plt.savefig('figureLandmarkLego.png')
plt.show()