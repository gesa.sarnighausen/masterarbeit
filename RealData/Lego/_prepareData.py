import sys
sys.path.insert(0, 'C:/Users/gesas/PycharmProjects/masterarbeit/FilteredBackprojection')
import backprojection as backP
import numpy as np
import scipy.io as sio
import matplotlib.pyplot as plt

# load sinogram from matlab
mat_file_t4_16 = sio.loadmat('C:/Users/Uni/Mathe/2223 WS InverseProblemsProject/20221130_lego_t4_ct_project_2d_parallel_binning_16.mat')
sino_t4_16 = mat_file_t4_16['sinoParallel'].T
np.save('sino_end_16.npy', sino_t4_16)

mat_file_t4_4 = sio.loadmat('C:/Users/Uni/Mathe/2223 WS InverseProblemsProject/20221130_lego_t4_ct_project_2d_parallel_binning_4.mat')
sino_t4_4 = mat_file_t4_4['sinoParallel'].T
np.save('sino_end_4.npy', sino_t4_4)

mat_file_t3_16 = sio.loadmat('C:/Users/Uni/Mathe/2223 WS InverseProblemsProject/20221130_lego_t3_ct_project_2d_parallel_binning_16.mat')
sino_t3_16 = mat_file_t3_16['sinoParallel'].T

mat_file_t3_4 = sio.loadmat('C:/Users/Uni/Mathe/2223 WS InverseProblemsProject/20221130_lego_t3_ct_project_2d_parallel_binning_4.mat')
sino_t3_4 = mat_file_t3_4['sinoParallel'].T

mat_file_t2_16 = sio.loadmat('C:/Users/Uni/Mathe/2223 WS InverseProblemsProject/20221130_lego_t2_ct_project_2d_parallel_binning_16.mat')
sino_t2_16 = mat_file_t2_16['sinoParallel'].T

mat_file_t2_4 = sio.loadmat('C:/Users/Uni/Mathe/2223 WS InverseProblemsProject/20221130_lego_t2_ct_project_2d_parallel_binning_4.mat')
sino_t2_4 = mat_file_t2_4['sinoParallel'].T

mat_file_t1_16 = sio.loadmat('C:/Users/Uni/Mathe/2223 WS InverseProblemsProject/20221130_lego_t1_ct_project_2d_parallel_binning_16.mat')
sino_t1_16 = mat_file_t1_16['sinoParallel'].T
np.save('sino_start_16.npy', sino_t1_16)

mat_file_t1_4 = sio.loadmat('C:/Users/Uni/Mathe/2223 WS InverseProblemsProject/20221130_lego_t1_ct_project_2d_parallel_binning_4.mat')
sino_t1_4 = mat_file_t1_4['sinoParallel'].T
np.save('sino_start_4.npy', sino_t1_4)

mat_file_truth = sio.loadmat('C:/Users/Uni/Mathe/2223 WS InverseProblemsProject/20221130_lego_t1_ct_project_2d_groundtruth.mat')
groundtruth = mat_file_truth['reconAstra']
np.save('groundtruth.npy', groundtruth)

sino_dyn_4 = np.zeros_like(sino_t1_4)
sino_dyn_4[0:45] = sino_t1_4[0:45]
sino_dyn_4[45:90] = sino_t2_4[45:90]
sino_dyn_4[90:135] = sino_t3_4[90:135]
sino_dyn_4[135:180] = sino_t4_4[135:180]
np.save('sino_dyn_4.npy', sino_dyn_4)

fig = plt.imshow(sino_dyn_4, cmap='gray')
fig.axes.get_xaxis().set_visible(False)
fig.axes.get_yaxis().set_visible(False)
plt.savefig('dynSino.png')

sino_dyn_16 = np.zeros_like(sino_t1_16)
sino_dyn_16[0:45] = sino_t1_16[0:45]
sino_dyn_16[45:90] = sino_t2_16[45:90]
sino_dyn_16[90:135] = sino_t3_16[90:135]
sino_dyn_16[135:180] = sino_t4_16[135:180]
np.save('sino_dyn_16.npy', sino_dyn_16)

print('done')