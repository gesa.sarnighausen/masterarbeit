import numpy as np
import scipy
import matplotlib.pyplot as plt
import sys
import time

sys.path.insert(0, 'C:\\Users\\gesas\\PycharmProjects\\masterarbeit\\KaczmarzRESESOP_Reconstructions')
sys.path.insert(0, '/home/uni08/gsarnig/masterarbeit/KaczmarzRESESOP_Reconstructions')
import Resesop

sys.path.insert(0, 'C:\\Users\\gesas\\PycharmProjects\\masterarbeit\\FilteredBackprojection')
sys.path.insert(0, '/home/uni08/gsarnig/masterarbeit/FilteredBackprojection')
import dynamicBackprojection as dynFBP


test = scipy.constants.pi
resesop_size = 128
rec_size = 511



sino_start_4 = np.load('sino_start_4.npy')
sino_end_4 = np.load('sino_end_4.npy')
sino_dyn_4 = np.load('sino_dyn_4.npy')
sino_start_16 = np.load('sino_start_16.npy')
sino_end_16 = np.load('sino_end_16.npy')
sino_dyn_16 = np.load('sino_dyn_16.npy')


# estimate noise
start_time = time.time()
noise = np.mean(sino_dyn_16[:,:15]) * np.ones_like(sino_dyn_16)
#plt.imshow(sino_dyn_16, cmap='gray')
#plt.show()

# reconstruct with Resesop-Kaczmarz
eta_start = np.absolute(sino_start_16 - sino_dyn_16)
eta_end = np.absolute(sino_end_16 - sino_dyn_16)
delta = noise
rec_start, _ = Resesop.run_resesop(sino_dyn_16, eta_start, delta,
                                   reconstruction_size=resesop_size, max_iterations=3, not_bigger_1='False')
rec_end, _ = Resesop.run_resesop(sino_dyn_16, eta_end, delta,
                                 reconstruction_size=resesop_size, max_iterations=3, not_bigger_1='False')
end_time = time.time()
resesopHelp_time = end_time - start_time

f = open('time.txt', "a")
f.write('resesop for landmarkdetection:' + str(resesopHelp_time) + '\n')
f.close()

np.save('recResKac_start_' + str(resesop_size) + '.npy', rec_start)
plt.imshow(rec_start, cmap='gray')
plt.savefig('recResKac_start_' + str(resesop_size) + '.png')
plt.show()
plt.close()
np.save('recResKac_end_' + str(resesop_size) + '.npy', rec_end)
plt.imshow(rec_end, cmap='gray')
plt.savefig('recResKac_end_' + str(resesop_size) + '.png')
plt.close()

start_time = time.time()
noise = np.mean(sino_dyn_4[:,:15]) * np.ones_like(sino_dyn_4)
eta_start = np.absolute(sino_start_4 - sino_dyn_4)
delta = noise
rec_resesop, _ = Resesop.run_resesop(sino_dyn_4, eta_start, delta,
                                     reconstruction_size=rec_size, max_iterations=30,not_bigger_1='False')
end_time = time.time()
resesop_time = end_time - start_time
f = open('time.txt', "a")
f.write('resesop 30 iterations:' + str(resesop_time) + '\n')
f.close()

np.save('recResKac30iter.npy', rec_resesop)
plt.imshow(rec_resesop, cmap='gray', vmin=0, vmax=1)
plt.savefig('recResKac30iter.png')
plt.close()



rec_start = np.load('recResKac_start_128.npy')
rec_end = np.load('recResKac_end_128.npy')

figure, axis = plt.subplots(1, 2)
axis[0].imshow(rec_start, cmap='gray')
axis[1].imshow(rec_end, cmap='gray')
plt.show()

# Landmarkdetection by hand
# top left, top right, bottom left, bottom right [i,j]
StartCorners = np.array([[12, 77], [31,104],[53,51], [71,76]])
EndCorners = np.array([[53,51], [70,77],[93,25], [111,49]])

shift = np.mean((EndCorners - StartCorners), axis=0)


shift_corrected = shift * rec_size/resesop_size
#shift_corrected values [152.1875,-100.8242]

#plot detected landmarks
figure, axis = plt.subplots(1, 2)
axis[0].imshow(rec_start, cmap='gray', vmin=0, vmax=1)
axis[0].scatter(StartCorners[:, 1], StartCorners[:, 0], color='red', s=0.2)
axis[1].imshow(rec_end, cmap='gray', vmin=0, vmax=1)
axis[1].scatter(EndCorners[:, 1], EndCorners[:, 0], color='red', s=0.2)
axis[0].set_title('Start')
axis[1].set_title('End')
plt.show()




def b(t, no_of_timesteps=4, xshift=shift_corrected[1], yshift=shift_corrected[0]):
    if t < 45:
        time = 0
    elif t < 90:
        time = 1
    elif t < 135:
        time = 2
    else: #t < 180
        time = 3
    return time / (no_of_timesteps - 1) * np.array([xshift / rec_size * 2, -yshift / rec_size * 2])

def b_0(t):
    return np.zeros(2)

def I(t, images_per_video):
    return np.eye(2)


print('starting first reconstruction')
start_time = time.time()
rec = dynFBP.FBP_dynamic(sino_dyn_4, rec_size, I, dynFBP.derivative_A_inv_T_times_theta, b, dynFBP.gaussian_kernel)
end_time = time.time()
time_dynFBP = end_time - start_time
f = open('time.txt', "a")
f.write('dyn FBP:' + str(time_dynFBP) + '\n')
f.close()



np.save('recDynFBP.npy', rec)

rec = np.load('recDynFBP.npy')


print('starting second reconstruction')
start_time = time.time()
rec2 = dynFBP.FBP_dynamic(sino_dyn_4, rec_size, I, dynFBP.derivative_A_inv_T_times_theta, b_0, dynFBP.gaussian_kernel)
end_time = time.time()
time_fbp = end_time - start_time
f = open('time.txt', "a")
f.write('FBP:' + str(time_fbp) + '\n')
f.close()

np.save('recFBP.npy',rec2)

rec2 = np.load('recFBP.npy')


'''figure, axis = plt.subplots(1, 2)
axis[0].imshow(rec, cmap='gray', vmin=0, vmax=1)
axis[1].imshow(rec2, cmap='gray', vmin=0, vmax=1)
axis[0].set_title('With Shift')
axis[1].set_title('Without Shift')
plt.savefig('testFinalRec_' + str(rec_size) + '.png')
plt.show()'''


print('done')