import CNN6
from matplotlib import pyplot as plt
import torch
from torch import nn
import numpy as np
import CERDataset
from torch.utils.data import Dataset, DataLoader



def train_loop(dataloader, model, loss_fn, optimizer):
    num_batches = len(dataloader)
    size = len(dataloader.dataset)
    train_loss = 0
    for batch, sample in enumerate(dataloader):
        # Compute prediction and loss
        X = sample['image']
        y = sample['landmarks']
        X, y = X.to(device), y.to(device)
        pred = model(X)
        loss = loss_fn(pred, y)
        train_loss += loss_fn(pred, y).item()

        # Backpropagation
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

    train_loss /= size
    return train_loss


def validation_loop(dataloader, model, loss_fn):
    num_batches = len(dataloader)
    validation_loss = 0
    size = len(dataloader.dataset)

    with torch.no_grad():
        for sample in dataloader:
            X = sample['image']
            y = sample['landmarks']
            X, y = X.to(device), y.to(device)
            pred = model(X)
            validation_loss += loss_fn(pred, y).item()

    validation_loss /= size
    return validation_loss




if __name__ == "__main__":
    mode = 'pretrain2'
    # computations on gpu if possible
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    torch.cuda.empty_cache()
    torch.set_default_dtype(torch.float64)

    # load and shuffle data and define train/test dataloader
    if mode == 'pretrain1':
        landmarkdir = 'TrainingLabels1'
        imagedir = 'TrainingImages1'
        params_name = '1pretrain_params.pt'
        train_loss_name = '1pretrain_train_loss.npy'
        validation_loss_name = '1pretrain_val_loss.npy'
        figure_name = '1pretrain_loss.png'
    elif mode == 'pretrain2':
        landmarkdir = 'TrainingLabels1'
        imagedir = 'TrainingImages2'
        params_name = '2pretrain_params.pt'
        train_loss_name = '2pretrain_train_loss.npy'
        validation_loss_name = '2pretrain_val_loss.npy'
        figure_name = '2pretrain_loss.png'


    full_data = CERDataset.CERDataset(landmark_dir=landmarkdir, image_dir=imagedir)
    train_data_size = int(0.72 * len(full_data))
    test_data_size = int(0.18 * len(full_data))
    valid_data_size = len(full_data) - train_data_size - test_data_size
    assert train_data_size + test_data_size + valid_data_size == len(full_data)
    train_data, test_data, valid_data = torch.utils.data.random_split(full_data, [train_data_size, test_data_size, valid_data_size], generator=torch.Generator().manual_seed(42))

    train_dataloader = DataLoader(train_data, batch_size=8, shuffle=True)
    test_dataloader = DataLoader(test_data, batch_size=8, shuffle=True)
    valid_dataloader = DataLoader(valid_data, batch_size=8, shuffle=False)


    model = CNN6.CNN7()
    model.to(device)
    # load pretrained parameters for the model
    # change back to cuda
    if mode == 'pretrain2':
        model.load_state_dict(torch.load('1pretrain_params.pt', map_location=torch.device('cuda')))



    max_epochs = 10000

    # early stopping
    patience = 200
    epochsFromLastImprovement = 0

    # training
    validation_loss_min = 9999999
    validation_loss_list = []
    train_loss_list = []
    loss = nn.MSELoss()

    for t in range(max_epochs):
        if epochsFromLastImprovement > patience:
            break
        train_loss = train_loop(train_dataloader, model, loss,
                                torch.optim.Adam(model.parameters(), lr=1e-6))
        train_loss_list.append(train_loss)
        validation_loss = validation_loop(valid_dataloader, model, loss)
        if t % 25 == 0:
            print(f"Epoch {t + 1}\n-------------------------------")
            print(f"train_loss: {train_loss:>7f}")
            print(f"Validation Error: \n Avg loss: {validation_loss:>8f} \n")
        validation_loss_list.append(validation_loss)

        train_loss_np = np.array(train_loss_list)
        np.save(train_loss_name, train_loss_np)
        validation_loss_np = np.array(validation_loss_list)
        np.save(validation_loss_name, validation_loss_np)

        if validation_loss < validation_loss_min:
            torch.save(model.state_dict(), params_name)
            epochsFromLastImprovement = 0
            validation_loss_min = validation_loss
        else:
            epochsFromLastImprovement += 1
    print("Done!")

    # plot loss
    x = np.arange(1, t + 1)
    train_loss = np.array(train_loss_list)
    np.save(train_loss_name, train_loss)
    validation_loss = np.array(validation_loss_list)
    np.save(validation_loss_name, validation_loss)
    plt.plot(x, train_loss, label='trainloss')
    plt.plot(x, validation_loss, label='validation_loss')
    plt.legend()
    plt.savefig(figure_name)
