from torch import nn
import torch
import numpy as np

numOfLabels = 4
maxStage = 6


# input: tensor of size (minibatchsize, numberOfChannels=1, Length=64, Width=64)
# output: tensor of the same size
class CNN7(nn.Module):
    def __init__(self):
        super().__init__()
        self.pool = nn.MaxPool2d(2, stride=2)
        self.relu = nn.ReLU()

        self.conv1 = nn.Conv2d(in_channels=1, out_channels=16, kernel_size=3, padding='same')
        self.conv2 = nn.Conv2d(in_channels=16, out_channels=32, kernel_size=3, padding='same')
        self.conv3 = nn.Conv2d(in_channels=32, out_channels=64, kernel_size=3, padding='same')
        self.conv4 = nn.Conv2d(in_channels=64, out_channels=128, kernel_size=3, padding='same')
        self.conv5 = nn.Conv2d(in_channels=128, out_channels=256, kernel_size=3, padding='same')
        self.conv6 = nn.Conv2d(in_channels=256, out_channels=512, kernel_size=3, padding='same')
        self.fullConnected7 = nn.Linear(in_features=2048, out_features=1024, bias=True)
        self.output = nn.Linear(in_features=1024, out_features=8, bias=True)

    def forward(self, x):
        temp = self.pool(self.relu(self.conv1(x)))
        temp = self.pool(self.relu(self.conv2(temp)))
        temp = self.pool(self.relu(self.conv3(temp)))
        temp = self.pool(self.relu(self.conv4(temp)))
        temp = self.pool(self.relu(self.conv5(temp)))
        temp = self.pool(self.relu(self.conv6(temp)))

        temp = torch.flatten(temp, start_dim=1)
        temp = self.relu(self.fullConnected7(temp))
        temp = self.output(temp)
        return temp


'''# test that CNN-6 is working
random_data = torch.rand((20, 1, 128, 128))
my_nn = CNN7()
result = my_nn(random_data)
print(result.shape)'''


def WingLoss(label_pred, label_true):
    batch_size = label_pred.shape[0]
    label_size = label_pred.shape[1]
    loss = 0
    for b in range(batch_size):
        for l in range(label_size):
            loss += wing(label_pred[b, l] - label_true[b, l])
    return loss


def wing(x, w=10, eps=2):
    if abs(x) < w:
        return w * np.log(1 + abs(x) / eps)
    else:
        return abs(x) - w + w * np.log(1 + w / eps)

