## the function FBP_dynamic performing the dynamic filtered backprojection is the main function in this file

import numpy as np
from scipy.linalg import inv, det
from scipy.special import dawsn
import scipy.integrate as integrate
import matplotlib.pyplot as plt


# dawson integral
def dawson(x):
    d = np.zeros_like(x)
    for i in range(len(x)):
        d[i] = np.exp(-x[i] ** 2) * integrate.quad(lambda t: np.exp(t ** 2), 0, x[i])[0]
    return d

# several static filters

def shepp_logan(s, b):
    r = (np.pi / 2 - b * s * np.sin(b * s)) / ((np.pi / 2) ** 2 - (b * s) ** 2)
    r[np.isnan(r)] = 1 / np.pi
    return b ** 2 / (2 * np.pi ** 3) * r


def gauss_static(s, gamma):
    v1 = 1 / (4 * np.pi ** 2 * gamma ** 2) * (1 - np.sqrt(2) * s / gamma * dawsn(s / (gamma * np.sqrt(2))))
    return v1


def ram_lak(s, b):
    w_b = np.sin(b * s) / (b * s) - 1 / 2 * (np.sin(b * s / 2) / (b * s / 2)) ** 2
    w_b[np.abs(b * s) == 0] = 1 / 2
    w_b *= b ** 2 / (4 * np.pi ** 2)
    return w_b

# dynamic FBP
# input:
# sinogram: np.array of size NumOfAngles x NumOfOffsetvalues
# NumOfAngles: number of equidistantly space scanning angles between 0 and pi
# NumOfOffsetvalues: number of equidistantly spaced offsetvalues between -1 and 1
# NumOfPixels: the reconstruction is of size NumOfPixels x NumOfPixels
# A: motion matrix A as a function with parameters: index of current angle, NumberOfAngles
# derivative_A_inv_T_times_theta: a function that returns the derivative of A^{-1} * theta, in the case of the motion type considered in the thesis, the function 'derivative_A_inv_T_times_theta' from this file can be used
# b: motion vector b as a function with parameters: index of current angle, NumberOfAngles
# Psi_gamma: filter, the function 'gaussian_kernel' from this file can be used 
def FBP_dynamic(sinogram, NumOfPixels, A, derivative_A_inv_T_times_theta, b, Psi_gamma):
    assert NumOfPixels % 2 == 1
    NumOfAngles = np.shape(sinogram)[0]
    NumOfOffsetvalues = np.shape(sinogram)[1]
    q = int((NumOfOffsetvalues - 1) / 2)
    # length of a pixel
    delta = 2 / NumOfPixels
    h = 1 / q
    s = np.linspace(-1, 1, NumOfOffsetvalues, endpoint=True)
    phi = np.linspace(0, np.pi, NumOfAngles, endpoint=False)
    Phi_const = phi[1]
    theta = np.array([np.cos(phi), np.sin(phi)]).T

    v = np.zeros((NumOfAngles, NumOfOffsetvalues))

    for i in range(NumOfAngles):
        for k in range(NumOfOffsetvalues):
            res = Psi_gamma(theta=theta[i], s=np.ones_like(s) * s[k] - s, A=A(i, NumOfAngles),
                                     b=b(i,NumOfAngles), gamma=h,
                                     derivative_A_inv_T_times_theta=derivative_A_inv_T_times_theta(phi[i], Phi_const,
                                                                                                   NumOfAngles,
                                                                                                   A(i, NumOfAngles)))
            v[i, k] = h * np.sum(res * sinogram[i, :])

    f_fbt = np.zeros((NumOfPixels, NumOfPixels))
    testArray = np.zeros(50)
    for i in range(NumOfPixels):
        # the coordinates (x_1,x_2) are in the center of each pixel
        x_2 = 1 - delta * (i + 1 / 2)
        for j in range(NumOfPixels):
            x_1 = -1 + delta * (j + 1 / 2)
            summe = 0
            if x_1 ** 2 + x_2 ** 2 > 1:  # coordinates outside the unit circle don't matter
                pass
            else:
                for l in range(NumOfAngles):
                    s = np.inner(inv(A(l, NumOfAngles)) @ np.array([x_1, x_2]), theta[l])
                    k = int(np.floor(s * q)) + q
                    if k >= 0 and k < NumOfOffsetvalues - 1:
                        u = s * q - k + q
                        assert u >= 0
                        assert u <= 1
                        summe += (1 - u) * v[l, k] + u * v[l, k + 1]
                    elif k >= NumOfOffsetvalues -1:
                        summe += v[l,-1]
                    elif k < 0:
                        summe += v[l,0]
            f_fbt[i, j] = 2 * np.pi / NumOfAngles * summe
    return f_fbt


def gaussian_kernel(theta, s, A, b, gamma, derivative_A_inv_T_times_theta):
    A_inv = inv(A)
    A_inv_T = A_inv.transpose()
    A_inv_T_times_theta = A_inv_T @ theta
    norm = np.linalg.norm(A_inv_T_times_theta)
    h = A_inv_T_times_theta[0] * derivative_A_inv_T_times_theta[1] - A_inv_T_times_theta[1] * \
        derivative_A_inv_T_times_theta[0]
    prefac = (np.abs(det(A)) * np.abs(h)) / (4 * np.pi ** 2 * gamma ** 2 * norm ** 2)
    dawsFac = (s + np.inner(A_inv @ b, theta)) / (gamma * norm)
    res = prefac * (1 - np.sqrt(2) * dawsFac * dawsn(dawsFac / np.sqrt(2)))
    return res


def derivative_A_inv_T_times_theta(phi, Phi_const, num_of_im, A):
    a = A[0, 0]
    b = A[0, 1]
    c = A[1, 0]
    d = A[1, 1]
    u1 = (Phi_const * (num_of_im - 1) + phi * (d - 1)) * np.cos(phi) - phi * c * np.sin(phi)
    u1_der = (d - 1) * np.cos(phi) - np.sin(phi) * (Phi_const * (num_of_im - 1) + phi * (d - 1)) - c * np.sin(phi) - np.cos(
        phi) * phi * c
    u2 = (Phi_const * (num_of_im - 1) + phi * (a - 1)) * np.sin(phi) - phi * b * np.cos(phi)
    u2_der = np.sin(phi) * phi * b - b * np.cos(phi) + np.cos(phi) * (Phi_const * (num_of_im - 1) + phi * (a - 1)) + (
            a - 1) * np.sin(phi)
    v = Phi_const ** 2 * (num_of_im - 1) ** 2 + Phi_const * (num_of_im - 1) * phi * (a + d - 2) + phi ** 2 * (
            (a - 1) * (d - 1) - b * c)
    v_der = Phi_const * (num_of_im - 1) * (a + d - 2) + 2 * phi * ((a - 1) * (d - 1) - b * c)
    der_1 = (u1_der * v - u1 * v_der) / (v ** 2)
    der_2 = (u2_der * v - u2 * v_der) / (v ** 2)
    return Phi_const * (num_of_im - 1) * np.array([der_1, der_2])
