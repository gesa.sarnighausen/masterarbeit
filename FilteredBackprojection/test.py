import numpy as np
import scipy
import matplotlib.pyplot as plt
import sys

sys.path.insert(0, '/home/gsarnig/Documents/Studium/PythonProjects/masterarbeit')
import dynamicBackprojection as dynFBP

sys.path.insert(0, '/home/gsarnig/Documents/Studium/PythonProjects/masterarbeit/Phantoms/Dynamic')
import DynamicGivenTransform

sys.path.insert(0, '/home/gsarnig/Documents/Studium/PythonProjects/masterarbeit/Sinograms')

import sino

test = scipy.constants.pi

phantom_size = 150
NumOfOffsetValues = 90
NumOfAngles = 135
rec_size = 135

'''create phantom'''
phantom = np.zeros((phantom_size, phantom_size))
phantom[45:75, 70:80] = 0.6


plt.imshow(phantom, cmap='gray', vmin=0, vmax=1)
plt.savefig('testphantom_' + str(phantom_size) + '.png')
plt.close()
np.save('testphantom_' + str(phantom_size) + '.npy', phantom)

# create dynamic test phantom with matrix
# x stretch

A_b = np.array([[2, 0, 0], [0, 1, 0], [0, 0, 1]])
gif, phantom_dynamic, notunitcircle = DynamicGivenTransform.dynamic(images_per_video=NumOfAngles, phantom=255 * phantom,
                                                                    A_b=A_b)
gif[0].save('testDynamic_' + str(phantom_size) + '.gif', save_all=True, append_images=gif[1:], duration=50, loop=0)
np.save('testDynamic_' + str(phantom_size) + '.npy', phantom_dynamic)

'''create sinogram'''
sino = sino.radon_dynamic(phantom_dynamic, NumOfOffsetvalues=NumOfOffsetValues)

'''reconstruct'''

def b_0(t, images_per_video):
    return np.zeros(2)

def A(t, images_per_video):
    return np.linalg.inv(np.eye(2) + t/(images_per_video-1) * (np.array([[2, 0], [0, 1]]) - np.eye(2)))

def A(t, images_per_video):
    return np.eye(2)


rec = dynFBP.FBP_dynamic(sino, rec_size, A, dynFBP.derivative_A_inv_T_times_theta, b_0, dynFBP.gaussian_kernel)
plt.imshow(rec, cmap='gray', vmin=0, vmax=1)
plt.show()
print('done')