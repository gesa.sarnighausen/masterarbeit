import numpy as np
import matplotlib.pyplot as plt
from scipy.special import dawsn

# FBP
# input:
# sinogram: np.array of size NumOfAngles x NumOfOffsetvalues
# NumOfAngles: number of equidistantly space scanning angles between 0 and pi
# NumOfOffsetvalues: number of equidistantly spaced offsetvalues between -1 and 1
# NumOfPixels: the reconstruction is of size NumOfPixels x NumOfPixels
# w_b: filter, the functions 'shepp_logan', 'gauss_static' or 'ram_lak' from this file can be used
def FBP(sinogram, NumOfPixels, w_b):
    assert NumOfPixels % 2 == 1
    NumOfAngles = np.shape(sinogram)[0]
    NumOfOffsetvalues = np.shape(sinogram)[1]
    q = int((NumOfOffsetvalues - 1) / 2)
    # length of a pixel
    delta = 2 / NumOfPixels
    h = 1 / q
    s = np.linspace(-1, 1, NumOfOffsetvalues, endpoint=True)

    v = np.zeros((NumOfAngles, NumOfOffsetvalues))
    for i in range(NumOfAngles):
        for k in range(NumOfOffsetvalues):
            # parameter gamma/b depends on filter
            if w_b == gauss_static:
                v[i, k] = h * np.sum(w_b(np.ones_like(s) * s[k] - s, h) * (sinogram[i, :]))
            else:  # ram_lak or shepp_logan
                v[i, k] = h * np.sum(w_b(np.ones_like(s) * s[k] - s, np.pi / h) * (sinogram[i, :]))

    f_fbt = np.zeros((NumOfPixels, NumOfPixels))

    for i in range(NumOfPixels):
        # the coordinates (x,y) are in the center of each pixel
        y = 1 - delta * (i + 1 / 2)
        for j in range(NumOfPixels):
            x = -1 + delta * (j + 1 / 2)
            sum = 0
            if x ** 2 + y ** 2 > 1:  # coordinates outside the unit circle don't matter
                pass
            else:
                for l in range(NumOfAngles):
                    omega = np.array([np.cos(l * np.pi / NumOfAngles), np.sin(l * np.pi / NumOfAngles)])
                    s = omega[0] * x + omega[1] * y
                    k = int(np.floor(s * q)) + q
                    mu = s * q - k + q
                    assert mu >= 0
                    assert mu <= 1
                    '''if x**2+y**2 > 1: # coordinates outside the unit circle don't matter
                        pass
                    else:'''
                    sum += (1 - mu) * v[l, k] + mu * v[l, k + 1]
            f_fbt[i, j] = 2 * np.pi / NumOfAngles * sum
    return f_fbt


def shepp_logan(s, b):
    r = (np.pi / 2 - b * s * np.sin(b * s)) / ((np.pi / 2) ** 2 - (b * s) ** 2)
    r[np.isnan(r)] = 1 / np.pi
    return b ** 2 / (2 * np.pi ** 3) * r


def gauss_static(s, gamma):
    v1 = 1 / (4 * np.pi ** 2 * gamma ** 2) * (1 - np.sqrt(2) * s / gamma * dawsn(s / (gamma * np.sqrt(2))))
    return v1


def ram_lak(s, b):
    w_b = np.sin(b * s) / (b * s) - 1 / 2 * (np.sin(b * s / 2) / (b * s / 2)) ** 2
    w_b[np.abs(b * s) == 0] = 1 / 2
    w_b *= b ** 2 / (4 * np.pi ** 2)
    return w_b




